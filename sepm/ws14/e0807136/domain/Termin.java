package sepm.ws14.e0807136.domain;

import java.sql.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Klasse der Entitiaet Termin
 * @author Saskia Reimerth, 0807136
 */
public class Termin {
	
	private static final Logger log = LogManager.getLogger(Termin.class);
	
	private Pferd pferd;
	private Date datum;
	private boolean ausritt;
	private boolean isStorno;

	/**
	 * Konstruktoren
	 */
	public Termin(){}
	
	public Termin(Pferd pferd, Date datum, boolean ausritt,
			boolean isStorno) {
		log.info("Enter Termin Construktor");
		this.setAusritt(ausritt);
		this.pferd = pferd;
		this.datum = datum;
		this.isStorno = isStorno;
	}


	/**
	 * Getter und Setter
	 */
	public Pferd getPferd() {
		return pferd;
	}
	public void setPferd(Pferd pferd) {
		this.pferd = pferd;
	}
	public Date getDatum() {
		return datum;
	}
	public void setDatum(Date datum) {
		this.datum = datum;
	}
	public boolean isStorno() {
		return isStorno;
	}
	public void setStorno(boolean isStorno) {
		this.isStorno = isStorno;
	}
	public boolean isAusritt() {
		return ausritt;
	}
	public void setAusritt(boolean ausritt) {
		this.ausritt = ausritt;
	}
	
	public String toString(){
		StringBuilder builder = new StringBuilder();
		if(isStorno){
			builder.append("(STORNIERT) ");
		}
		builder.append("Pferd: " + pferd.getName() + ", ");
		builder.append("Datum: " + datum.toString() + ", Ausritt: ");
		if(ausritt){
			builder.append("Ja\n");
		} else {
			builder.append("Nein\n");
		}
		
		return builder.toString();
	}
}
