package sepm.ws14.e0807136.domain;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Klasse der Entitiaet Pferd
 * @author Saskia Reimerth, 0807136
 */
public class Pferd {

	private static final Logger log = LogManager.getLogger(Pferd.class);
	
	private int id = -1;
	private String name = null;
	private int jahre = -1;
	private double hoehe = 0.0;
	private int gewicht = 0;
	private String foto = null;
	private boolean isActive = true;
	
	
	/**
	 * Konstruktoren
	 */
	public Pferd(){}
	
	public Pferd(int id, String name, int jahre, double hoehe, int gewicht,
			String foto, boolean isActive) {
		log.info("Enter Konstruktor: Pferd");
		this.id = id;
		this.name = name;
		this.jahre = jahre;
		this.hoehe = hoehe;
		this.gewicht = gewicht;
		this.foto = foto;
		this.isActive = isActive;
	}
	
	
	/**
	 * Getter und Setter
	 */
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getJahre() {
		return jahre;
	}
	public void setJahre(int jahre) {
		this.jahre = jahre;
	}
	public double getHoehe() {
		return hoehe;
	}
	public void setHoehe(double hoehe) {
		this.hoehe = hoehe;
	}
	public int getGewicht() {
		return gewicht;
	}
	public void setGewicht(int gewicht) {
		this.gewicht = gewicht;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public void setActive(boolean b){
		this.isActive = b;
	}
	public boolean getActive(){
		return isActive;
	}
	
	/*
	 * Gibt den Namen des Pferdes zurueck
	 */
	@Override
	public String toString() {
		return "Pferd: " + name + "]";
	}
	
	
	
}
