package sepm.ws14.e0807136.domain;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Klasse der Entitiaet Buchung
 * @author Saskia Reimerth, 0807136
 */
public class Buchung {

	private static final Logger log = LogManager.getLogger(Buchung.class);

	private int id;
	private String kunde;
	private Collection<Termin> termine = new ArrayList<Termin>();

	
	/**
	 * Konstruktoren
	 */
	public Buchung(){}
	
	public Buchung(int id, String kunde) {
		log.info("Enter Konstruktor: Buchung");
		this.id = id;
		this.kunde = kunde;
	}
	
	
	/**
	 * Getter und Setter
	 */
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKunde() {
		return kunde;
	}
	public void setKunde(String kunde) {
		this.kunde = kunde;
	}

	public Collection<Termin> getTermine() {
		return termine;
	}

	public void setTermine(Collection<Termin> termine) {
		this.termine = termine;
	}	
	
	public void addTermin(Termin t){
		termine.add(t);
	}

	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("Kunde: " + kunde + "\n");
		builder.append("Ausgestellt am: " + new Date(System.currentTimeMillis()).toString() + "\n");
		if(termine.isEmpty()){
			builder.append("Zu dieser Buchung sind keine Termine vorhanden.\n");
			return builder.toString();
		} 
		for(Termin t : termine){
			builder.append(t.toString());
		}
		return builder.toString();
	}
	
	
}
