package sepm.ws14.e0807136.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Klasse fuer die Verbindung der Datenbank
 * Dazu zaehlen der Aufbau, die Verwaltung sowie das Schliessen der Verbindung
 * @author Saskia Reimerth, 0807136
 */
public class DBConnection {

	private static final Logger log = LogManager.getLogger(DBConnection.class);
	
	/**
	 * Die Datenbankverbindung
	 */
	private static Connection connection = null;
	
	/**
	 * Der Konstruktor
	 */
	public DBConnection(){}
	
	/**
	 * Stellt die Verbindung zur Datenbank her
	 * @throws SQLException
	 */
	private static void openConnection() throws SQLException{
		try {
			Class.forName("org.h2.Driver");
		} catch(Exception e) {
			log.error("Failed to load H2 Driver: " + e);
			throw new RuntimeException();
		}
		try{
			connection = DriverManager.getConnection("jdbc:h2:tcp://localhost/~/db", "sa", "");
		} catch (SQLException e){
			log.error("DBConnection Connection: "+e);
			throw new RuntimeException();
		}
	}
	
	/**
	 * Wenn die Verbindung zur Datenbank besteht, wird diese zurueckgegeben.
	 * Ansonsten wird die Verbindung hergestellt und dann weitergegeben.
	 * @return Die Verbindung
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException{
		if(connection!=null){
			return connection;
		} else {
			openConnection();
			return connection;
		}
	}
	
	/**
	 * Schliesst die Verbindung zur Datenbank
	 * @throws SQLException
	 */
	public static void closeConnection() throws SQLException{
		if(connection!=null){
			connection.close();
			connection = null;
		}
	}
	
}
