package sepm.ws14.e0807136.db;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Main Klasse.
 * @author Saskia Reimerth, 0807136
 */
public class main {

	public static void main(String[] args) throws SQLException {
		
		DBConnection db = new DBConnection();
		
		Connection c = db.getConnection();
		
		db.closeConnection();
	}

}
