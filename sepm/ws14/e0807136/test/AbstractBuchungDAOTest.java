package sepm.ws14.e0807136.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Date;
import java.util.Collection;

import org.junit.Test;

import sepm.ws14.e0807136.dao.BuchungDAO;
import sepm.ws14.e0807136.dao.DAOException;
import sepm.ws14.e0807136.dao.PferdDAO;
import sepm.ws14.e0807136.domain.Buchung;
import sepm.ws14.e0807136.domain.Pferd;
import sepm.ws14.e0807136.domain.Termin;

/**
 * JUnit-Testklasse fuer BuchungDAOs.
 * @author Saskia Reimerth, 0807136
 *
 */
public class AbstractBuchungDAOTest {

protected BuchungDAO buchungDAO;
protected PferdDAO pferdDAO;
	
	/**
	 * Versucht ohne Uebergabe eine Buchung zu erstellen
	 * @throws DAOException erwartet
	 */
	@Test(expected = DAOException.class)
	public void createWithNull() throws DAOException{
		Buchung b = null;
		buchungDAO.create(b);
	}
	
	/**
	 * Erstellt eine korrekte Buchung in der Datenbank
	 * @throws DAOException im Fehlerfall
	 */
	@Test
	public void createWithValids() throws DAOException{
		Buchung b = new Buchung(0, "Hayato Hinteregger");
		Pferd p = pferdDAO.read(5);
		Termin t = new Termin(p, new Date(System.currentTimeMillis()), false, false);
		b.addTermin(t);
		Collection<Buchung> buchungen = buchungDAO.readAll();
		assertFalse(buchungen.contains(b));
		buchungDAO.create(b);
		assertTrue(buchungDAO.read(b.getId()) != null);
		assertFalse(buchungDAO.read(b.getId()).getTermine().isEmpty());
	}
	
	/**
	 * Versucht eine Buchung mit einer negativen ID auszulesen
	 * @throws DAOException erwartet
	 */
	@Test(expected = DAOException.class)
	public void readWithNegativeId() throws DAOException{
		buchungDAO.read(-1);
	}
	
	/**
	 * Liest eine Buchung mit einer vorhandenen ID aus
	 * @throws DAOException im Fehlerfall
	 */
	@Test
	public void readWithValidID() throws DAOException{
		Buchung b = buchungDAO.read(4);
		assertNotNull(b);
		assertEquals(4, b.getId());
		assertFalse(b.getTermine().isEmpty());
	}
	
	/**
	 * Versucht eine Buchung mit einer zu hohen/nicht existenten ID auszulesen
	 * @throws DAOException erwartet
	 */
	@Test (expected = DAOException.class)
	public void readWithTooHighID() throws DAOException{
		buchungDAO.read(23934);
	}
	
	/**
	 * Liest alle in der Datenbank vorhandenen Buchungen aus
	 * @throws DAOException im Fehlerfall
	 */
	@Test
	public void readAllCorrect() throws DAOException {
		Collection<Buchung> alleBuchungen = buchungDAO.readAll();
		assertEquals(10, alleBuchungen.size());
	}
	
	/**
	 * Storniert einen Termin
	 * @throws DAOException im Fehlerfall
	 */
	@Test
	public void stornoCorrect() throws DAOException {
		Buchung b = buchungDAO.read(9);
		int i = 1;
		for(Termin ter : b.getTermine()){
			if(i==3){ buchungDAO.storno(b, ter); }
			i++;
		}
		b = buchungDAO.read(9);
		i = 1;
		for(Termin ter : b.getTermine()){
			if(i==3){ assertTrue(ter.isStorno()); }
			i++;
		}
		
	}
	
}
