package sepm.ws14.e0807136.test;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Test;

import sepm.ws14.e0807136.dao.DAOException;
import sepm.ws14.e0807136.dao.PferdDAO;
import sepm.ws14.e0807136.domain.Pferd;
import sepm.ws14.e0807136.domain.Termin;

/**
 * Abstrakte Testklasse fuer PferdDAOs. Hier sind jeweils Tests fuer die CRUD-Methoden zu finden
 * @author Saskia Reimerth, 0807136
 */
public class AbstractPferdDAOTest {
	
	protected PferdDAO pferdDAO;
	
	/**
	 * Setzt das zu testende PferdDAO
	 * @param pferdDAO das uebergebene DAO
	 */
	protected void setPferdDAO(PferdDAO pferdDAO){
		this.pferdDAO = pferdDAO;
	}
	
	/**
	 * Versucht ein "leeres" Pferd in die Datenbank einzutragen
	 * @throws DAOException erwartet, da dies nicht moeglich ist
	 */
	@Test (expected = DAOException.class)
	public void createWithNull() throws DAOException{
		Pferd p = null;
		pferdDAO.create(p);
	}
	
	/**
	 * Fuegt ein gueltiges neues Pferd in die Datenbank ein
	 * @throws DAOException im Fehlerfall
	 */
	@Test
	public void createWithValids() throws DAOException{
		Pferd p = new Pferd(0, "Pico", 1, 120.5, 200, "foto x", true);
		Collection<Pferd> pferde = pferdDAO.readAll();
		assertFalse(pferde.contains(p));
		pferdDAO.create(p);
		assertTrue(pferdDAO.read(p.getId()) != null);
	}
	
	/**
	 * Versucht ein Pferd mit einer negativen ID auszulesen
	 * @throws DAOException erwartet, da es nur positive IDs gibt
	 */
	@Test(expected = DAOException.class)
	public void readWithNegativeID() throws DAOException {
		pferdDAO.read(-1);
	}
	
	/**
	 * Versucht ein Pferd mittels einer ID auszulesen
	 * @throws DAOException falls es nicht funktioniert
	 */
	@Test
	public void readWithValidID() throws DAOException{
		Pferd p = pferdDAO.read(3);
		assertNotNull(p);
		assertEquals(3, p.getId());
	}
	
	/**
	 * Versucht ein Pferd mit einer viel zu hohen ID auszulesen
	 * @throws DAOException erwartet, da die IDs nicht so hoch sein werden
	 */
	@Test (expected = DAOException.class)
	public void readWithTooHighID() throws DAOException{
		pferdDAO.read(23934);
	}
	
	/**
	 * Liest alle Pferde in der Datenbank aus
	 * @throws DAOException im Fehlerfall
	 */
	@Test
	public void readAllCorrect() throws DAOException {
		Collection<Pferd> allePferde = pferdDAO.readAll();
		assertEquals(10, allePferde.size());
	}

	
	/**
	 * Versucht ein Pferd zu bearbeiten, jedoch wird NULL uebergeben
	 * @throws DAOException erwartet, da das uebergebene Pferd null ist
	 */
	@Test (expected = DAOException.class)
	public void updateWithNull() throws DAOException{
		Pferd p = null;
		pferdDAO.update(p);
	}
	
	/**
	 * Bearbeitet ein gueltiges Pferd in der Datenbanl
	 * @throws DAOException im Fehlerfall
	 */
	@Test
	public void updateWithValids() throws DAOException{
		Pferd p = new Pferd(2, "PicoUpdate", 1, 120.5, 200, "foto x", true);
		pferdDAO.update(p);
		Pferd neu = pferdDAO.read(2);
		assertEquals(p.getId(), neu.getId());
		assertEquals(p.getName(), neu.getName());
		assertTrue(p.getHoehe() == neu.getHoehe());
		assertEquals(p.getGewicht(), neu.getGewicht());
		assertEquals(p.getJahre(), neu.getJahre());
		assertEquals(p.getFoto(), neu.getFoto());
	}
	
	/**
	 * Versucht ein nicht vorhandenes Pferd in der Datenbank zu bearbeiten
	 * @throws DAOException erwaret, da das Pferd nicht existiert
	 */
	@Test (expected = DAOException.class)
	public void updateNonExisting() throws DAOException{
		Pferd p = new Pferd(0, "Pico", 1, 120.5, 200, "foto x", true);
		pferdDAO.update(p);
	}
	
	/**
	 * Versucht ein "leeres" Pferd zu loeschen
	 * @throws DAOException erwartet, da null uebergeben wird
	 */
	@Test (expected = DAOException.class)
	public void deleteWithNull() throws DAOException{
		Pferd p = null;
		pferdDAO.delete(p);
	}
	
	/**
	 * Loescht ein gueltiges Pferd in der Datenbanl
	 * @throws DAOException im Fehlerfall
	 */
	@Test
	public void deleteWithValids() throws DAOException{
		Pferd p = pferdDAO.read(5);
		pferdDAO.delete(p);
		Collection<Pferd> pferde = pferdDAO.readAll();
		assertFalse(pferde.contains(p));
	}
	
	/**
	 * Loescht ein gueltiges Pferd in der Datenbanl
	 * @throws DAOException im Fehlerfall
	 */
	@Test(expected = DAOException.class)
	public void deleteWithValidsButFutureDates() throws DAOException{
		Pferd p = pferdDAO.read(2);
		pferdDAO.delete(p);
		Collection<Pferd> pferde = pferdDAO.readAll();
		assertFalse(pferde.contains(p));
	}
	
	/**
	 * Liest alle Termine eines Pferdes aus der Datenbank aus
	 * @throws DAOException im Fehlerfall
	 */
	@Test
	public void readTermineCorrect() throws DAOException{
		Pferd p = pferdDAO.read(4);
		Collection<Termin> termine = pferdDAO.readTermine(p);
		assertTrue(2==termine.size());
		
	}
}
