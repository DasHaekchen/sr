package sepm.ws14.e0807136.test;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.Collection;

import org.junit.Test;

import sepm.ws14.e0807136.domain.Buchung;
import sepm.ws14.e0807136.domain.Pferd;
import sepm.ws14.e0807136.service.IService;
import sepm.ws14.e0807136.service.ServiceException;

/**
 * Diese Testklasse testet jene Methode der Serviceschicht, welche nicht einfach
 * an die CRUD-Methoden der DAOs weiterleiten
 * @author Saskia Reimerth, 0807136
 */
public class AbstractServiceTest {

	protected IService s;
	
	/**
	 * Sucht Pferde mit gegebenen Parametern
	 * @throws ServiceException im Fehlerfall
	 */
	@Test
	public void suchePferdeWithValids() throws ServiceException{
		Collection<Pferd> pferde = s.suchePferde("wind", 0, 15, 100, 500, 0, 0);
		assertFalse(pferde.isEmpty());
	}
	
	/**
	 * Sucht nach Pferden mit Parametern, welche keine Ergebnisse liefern
	 * @throws ServiceException im Fehlerfall
	 */
	@Test
	public void suchePferdeWithNoFindings() throws ServiceException{
		Collection<Pferd> pferde = s.suchePferde("kranich", 0, 15, 100, 500, 0, 0);
		assertTrue(pferde.isEmpty());
	}
	
	/**
	 * Ueberprueft die Verfuegbarkeit eines Pferdes
	 * @throws ServiceException im Fehlerfall
	 */
	@Test
	public void checkAvailabilityWithValids() throws ServiceException{
		Pferd p = s.readPferd(3);
		Date date = Date.valueOf("2014-12-12");
		boolean check = s.checkAvailability(p, date);
		assertTrue(check);
	}
	
	/**
	 * Versucht die Verfuegbarkeit zu ueberpruefen, jedoch ohne Parameter zu uebergeben
	 * @throws ServiceException erwartet
	 */
	@Test(expected = ServiceException.class)
	public void checkAvailabilityWithNull() throws ServiceException{
		Pferd p = null;
		Date date = null;
		s.checkAvailability(p, date);
	}
	
	/**
	 * Versucht ohne eine uebergebene Buchung eine Bestaetigung zu erstellen
	 * @throws ServiceException erwartet
	 */
	@Test(expected = ServiceException.class)
	public void getBestaetigungWithNull() throws ServiceException{
		s.getBestaetigung(null);
	}
	
	/**
	 * Erstellt eine Bestaetigung zu einer gegebenen Buchung
	 * @throws ServiceException im Fehlerfall
	 */
	@Test
	public void getBestaetigungWithValids() throws ServiceException{
		Buchung b = s.readBuchung(1);
		String best = s.getBestaetigung(b);
		assertNotNull(best);
	}
	
	
}
