package sepm.ws14.e0807136.test;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Stellt die Testdaten fuer die JUnit-Tests zur Verfuegung
 * @author Saskia Reimerth, 0807136
 */
public class TestData {
	
	/**
	 * Loescht die alten Daten und fuegt neue wieder ein, nachdem es die IDs wieder bei 1 starten laesst.
	 * @param c die Datenbank-Connection
	 * @throws SQLException bei einem Error seitens der Datenbank
	 */
	public static void data(Connection c) throws SQLException{
		c.prepareStatement("DELETE FROM Termin").execute();
		c.prepareStatement("DELETE FROM Buchung").execute();
		c.prepareStatement("DELETE FROM Pferd").execute();
		c.prepareStatement("ALTER TABLE Pferd ALTER COLUMN pferd_id RESTART WITH 1").execute();
		c.prepareStatement("ALTER TABLE Buchung ALTER COLUMN buchung_id RESTART WITH 1").execute();
		c.prepareStatement("INSERT INTO Pferd (name, jahre, hoehe, gewicht, foto, isActive) VALUES ('Glocke', '12', '160.5', '655', 'foto1', 'true');").execute();
		c.prepareStatement("INSERT INTO Pferd (name, jahre, hoehe, gewicht, foto, isActive) VALUES ('Bandit', '5', '155.5', '420', 'foto2', 'true');").execute();
		c.prepareStatement("INSERT INTO Pferd (name, jahre, hoehe, gewicht, foto, isActive) VALUES ('Hannah', '10', '180.8', '780', 'foto3', 'true');").execute();
		c.prepareStatement("INSERT INTO Pferd (name, jahre, hoehe, gewicht, foto, isActive) VALUES ('Sturmwind', '7', '174.0', '600', 'foto4', 'true');").execute();
		c.prepareStatement("INSERT INTO Pferd (name, jahre, hoehe, gewicht, foto, isActive) VALUES ('Keks', '8', '177.9', '685', 'foto5', 'true');").execute();
		c.prepareStatement("INSERT INTO Pferd (name, jahre, hoehe, gewicht, foto, isActive) VALUES ('Henrietta', '5', '137.5', '354', 'foto6', 'true');").execute();
		c.prepareStatement("INSERT INTO Pferd (name, jahre, hoehe, gewicht, foto, isActive) VALUES ('Lumberjack', '14', '191.2', '805', 'foto7', 'true');").execute();
		c.prepareStatement("INSERT INTO Pferd (name, jahre, hoehe, gewicht, foto, isActive) VALUES ('Punktpunktstrich', '4', '165.4', '575', 'foto8', 'true');").execute();
		c.prepareStatement("INSERT INTO Pferd (name, jahre, hoehe, gewicht, foto, isActive) VALUES ('Yoda', '14', '154.8', '458', 'foto9', 'true');").execute();
		c.prepareStatement("INSERT INTO Pferd (name, jahre, hoehe, gewicht, foto, isActive) VALUES ('Wanda', '11', '188.5', '724', 'foto10', 'true');").execute();
		c.prepareStatement("INSERT INTO Buchung (kunde) VALUES ('Karl der Gro�e');").execute();
		c.prepareStatement("INSERT INTO Buchung (kunde) VALUES ('Mila Superstar');").execute();
		c.prepareStatement("INSERT INTO Buchung (kunde) VALUES ('Jeanna Darc');").execute();
		c.prepareStatement("INSERT INTO Buchung (kunde) VALUES ('Arthur Conan Doyle');").execute();
		c.prepareStatement("INSERT INTO Buchung (kunde) VALUES ('J.R.R.R. Tolkien');").execute();
		c.prepareStatement("INSERT INTO Buchung (kunde) VALUES ('Arthur Dent');").execute();
		c.prepareStatement("INSERT INTO Buchung (kunde) VALUES ('Fort Perfect');").execute();
		c.prepareStatement("INSERT INTO Buchung (kunde) VALUES ('Morgan Freeman');").execute();
		c.prepareStatement("INSERT INTO Buchung (kunde) VALUES ('Ran Mori');").execute();
		c.prepareStatement("INSERT INTO Buchung (kunde) VALUES ('Kimi Reikk�nen');").execute();
		c.prepareStatement("INSERT INTO Termin (buchung_id, pferd_id, datum, ausritt, isStorno) VALUES (1, 3, '2014-10-31', true, false);").execute();
		c.prepareStatement("INSERT INTO Termin (buchung_id, pferd_id, datum, ausritt, isStorno) VALUES (1, 3, '2014-11-28', false, false);").execute();
		c.prepareStatement("INSERT INTO Termin (buchung_id, pferd_id, datum, ausritt, isStorno) VALUES (2, 8, '2014-11-15', true, true);").execute();
		c.prepareStatement("INSERT INTO Termin (buchung_id, pferd_id, datum, ausritt, isStorno) VALUES (4, 1, '2014-12-07', true, false);").execute();
		c.prepareStatement("INSERT INTO Termin (buchung_id, pferd_id, datum, ausritt, isStorno) VALUES (6, 7, '2015-01-07', false, false);").execute();
		c.prepareStatement("INSERT INTO Termin (buchung_id, pferd_id, datum, ausritt, isStorno) VALUES (9, 2, '2014-12-25', true, true);").execute();
		c.prepareStatement("INSERT INTO Termin (buchung_id, pferd_id, datum, ausritt, isStorno) VALUES (9, 2, '2015-01-18', false, false);").execute();
		c.prepareStatement("INSERT INTO Termin (buchung_id, pferd_id, datum, ausritt, isStorno) VALUES (9, 2, '2014-11-20', true, false);").execute();
		c.prepareStatement("INSERT INTO Termin (buchung_id, pferd_id, datum, ausritt, isStorno) VALUES (7, 4, '2014-10-25', false, false);").execute();
		c.prepareStatement("INSERT INTO Termin (buchung_id, pferd_id, datum, ausritt, isStorno) VALUES (7, 4, '2015-01-01', false, false);").execute();
	}
}
