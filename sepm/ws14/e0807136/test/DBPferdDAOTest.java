package sepm.ws14.e0807136.test;

import java.sql.Connection;

import org.junit.After;
import org.junit.Before;

import sepm.ws14.e0807136.db.DBConnection;
import sepm.ws14.e0807136.persistence.DBPferdDAO;

/**
 * Testklasse zum Testen des DBPferdDAOs. Setzt das eigentliche DAO und die Datenbank
 * @author Saskia Reimerth, 0807136
 */
public class DBPferdDAOTest extends AbstractPferdDAOTest{

	private Connection c;
	
	/**
	 * Wird vor den Tests ausgefuehrt und bereitet die noetigen Ressourcen
	 * (Datasource und das getestete PferdDAO) vor.
	 * @throws Exception falls dabei etwas schief lauft
	 */
	@Before
	public void setUp() throws Exception {
		c = DBConnection.getConnection();
		c.setAutoCommit(false);
		pd = new DBPferdDAO(c);
		TestData.data(c);
	}

	/**
	 * Nach den Tests werden die Veraenderungen an der DataSource wieder
	 * rueckgaengig gemacht
	 * @throws Exception
	 */
	@After
	public void tearDown() throws Exception {
		c.rollback();
	}
	

}
