package sepm.ws14.e0807136.dao;

import java.util.Collection;

import sepm.ws14.e0807136.domain.Pferd;
import sepm.ws14.e0807136.domain.Termin;

/**
 * Interface des Data Access Objects fuer Pferde
 * @author Saskia Reimerth, 0807136
 */
public interface PferdDAO {

	/**
	 * Erstellt ein Pferd mit den in "Pferd p" uebergebenen Werten
	 * @param p das zu erstellende Pferd
	 * @throws DAOException
	 */
	public void create(Pferd p) throws DAOException;
	
	/**
	 * Liest ein Pferd aus und gibt es zurueck
	 * @param i die Id des gesuchten Pferdes
	 * @return das gefundene Pferd
	 * @throws DAOException
	 */
	public Pferd read(int i) throws DAOException;
	
	/**
	 * Liest alle existierenden (nicht geloeschten) Pferde aus
	 * @return eine Collection mit allen noch vorhandenen Pferden
	 * @throws DAOException
	 */
	public Collection<Pferd> readAll() throws DAOException;
	
	/**
	 * Aendert die Parameter eines Pferdes
	 * @param p Die neuen Parameter des Pferdes
	 * @throws DAOException
	 */
	public void update(Pferd p) throws DAOException;
	
	/**
	 * Loescht ein Pferd
	 * @param p das zu loeschende Pferd
	 * @throws DAOException
	 */
	public void delete(Pferd p) throws DAOException;
	
	/**
	 * Liest alle Termine eines Pferdes aus
	 * @param p das Pferd
	 * @throws DAOException im Fehlerfall
	 */
	public Collection<Termin> readTermine(Pferd p) throws DAOException;
}
