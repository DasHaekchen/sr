package sepm.ws14.e0807136.dao;

/**
 * Eigene Exception fuer DAO-Ausnahmen
 * @author Saskia Reimerth, 0807136
 */
public class DAOException extends Exception{

	/**
	 * Default serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Gibt die Exception der Super-Klasse weiter.
	 */
	public DAOException(){
		super();
	}
	
	/**
	 * Gibt die Exception mit der Meldung der Super-Klasse weiter
	 * @param ex die mitgegebene Fehlermeldung
	 */
	public DAOException(String ex){
		super(ex);
	}
	
}
