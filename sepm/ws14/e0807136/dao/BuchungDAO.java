package sepm.ws14.e0807136.dao;

import java.util.Collection;

import sepm.ws14.e0807136.domain.Buchung;
import sepm.ws14.e0807136.domain.Termin;

/**
 * Interface des Data Access Objects fuer Buchungen
 * @author Saskia Reimerth, 0807136
 */
public interface BuchungDAO {

	/**
	 * Erstellt eine Buchung
	 * @param b die zu erstellende Buchung
	 * @throws DAOException
	 */
	public void create(Buchung b) throws DAOException;
	
	/**
	 * Liest eine Buchung aus
	 * @param i die ID der gesuchten Buchung
	 * @return die gesuchte (und gefundene) Buchung
	 * @throws DAOException
	 */
	public Buchung read(int i) throws DAOException;
	
	/**
	 * Nur der Vollstaendigkeit halber:
	 * Wuerde eine Buchung aendern
	 */
	public void update(Buchung b) throws DAOException;
	
	/**
	 * Nur der Vollstaendigkeit halber:
	 * Wuerde eine Buchung loeschen
	 */
	public void delete(Buchung b) throws DAOException;

	/**
	 * Liest alle Buchungen aus der Datenbank aus
	 * @return Collection aller Buchungen
	 * @throws DAOException
	 */
	public Collection<Buchung> readAll() throws DAOException;
	
	/**
	 * Storniert einen Termin t einer Buchung b.
	 * @throws DAOException
	 */
	public void storno(Buchung b, Termin t) throws DAOException;
}
