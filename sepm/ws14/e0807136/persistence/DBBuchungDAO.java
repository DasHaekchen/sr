package sepm.ws14.e0807136.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sepm.ws14.e0807136.dao.BuchungDAO;
import sepm.ws14.e0807136.dao.DAOException;
import sepm.ws14.e0807136.dao.PferdDAO;
import sepm.ws14.e0807136.domain.Buchung;
import sepm.ws14.e0807136.domain.Termin;

/**
 * Persistenz-Klasse des BuchungDAOs
 * @author Saskia Reimerth, 0807136
 */
public class DBBuchungDAO implements BuchungDAO{

	private static final Logger log = LogManager.getLogger(DBBuchungDAO.class);
	
	private PreparedStatement create;
	private PreparedStatement readAll;
	private PreparedStatement read;
	private PreparedStatement getId;
	private PreparedStatement updateStorno;
	private PreparedStatement createTermin;
	private PreparedStatement readTermin;
	
	private PferdDAO pferdDAO;
	
	
	public DBBuchungDAO(Connection c) throws DAOException{
		log.info("Konstruktor DBBuchungDAO");
		try {
			pferdDAO = new DBPferdDAO(c);
			create = c.prepareStatement("INSERT INTO Buchung VALUES (DEFAULT, ?);");
			readAll = c.prepareStatement("SELECT * FROM Buchung");
			read = c.prepareStatement("SELECT * FROM Buchung WHERE buchung_id = ?;");
			getId = c.prepareStatement("SELECT TOP 1 buchung_id FROM Buchung ORDER BY buchung_id DESC;");
			updateStorno = c.prepareStatement("UPDATE Termin SET isStorno = TRUE WHERE buchung_id = ? AND datum = ?;");
			createTermin = c.prepareStatement("INSERT INTO Termin VALUES (?, ?, ?, ?, false);");
			readTermin = c.prepareStatement("SELECT * FROM Termin WHERE buchung_id = ? ORDER BY datum;");
		} catch (SQLException ex) {
			log.error("Konstruktor mit Connection: " + c);
			throw new DAOException();
		}
	}
	

	@Override
	public void create(Buchung b) throws DAOException {
		log.debug("Entering CREATE with parameters: " + b);
		try {
			if (b == null) {
				throw new DAOException();
			}
			create.setString(1, b.getKunde());
			create.executeUpdate();

			ResultSet r = getId.executeQuery();
			if (!r.next()) {
				throw new DAOException();
			} else {
				b.setId(r.getInt("buchung_id"));
			}
			
			if(!b.getTermine().isEmpty()){
			//Termine hinzufuegen
			Collection<Termin> termine = b.getTermine();
			for (Termin t : termine){
				createTermin.setInt(1, b.getId());
				createTermin.setInt(2, t.getPferd().getId());
				createTermin.setDate(3, t.getDatum());
				createTermin.setBoolean(4, t.isAusritt());
				
				createTermin.executeUpdate();
			}}
			
		} catch (SQLException ex) {
			throw new DAOException();
		}
	}

	@Override
	public Buchung read(int i) throws DAOException {
		log.debug("Entering READ with parameter i: " + i);
		try {
			read.setInt(1, i);
			ResultSet r = read.executeQuery();
			if (!r.next()) {
				throw new DAOException();
			} else {
				Buchung b = new Buchung();
				b.setId(r.getInt("buchung_id"));
				b.setKunde(r.getString("kunde"));
				
				readTermin.setInt(1, b.getId());
				ResultSet rt = readTermin.executeQuery();
				
				while (rt.next()) {
					Termin t = new Termin();
					t.setPferd(pferdDAO.read(rt.getInt("pferd_id")));
					t.setDatum(rt.getDate("datum"));
					t.setAusritt(rt.getBoolean("ausritt"));
					t.setStorno(rt.getBoolean("isStorno"));
					b.addTermin(t);
				}
				
				return b;
			}
		} catch (SQLException ex) {
			throw new DAOException();
		}
	}

	@Override
	public void update(Buchung b) throws DAOException {
		log.debug("Entering UPDATE of Buchung, not available.");
	}

	@Override
	public void delete(Buchung b) throws DAOException {
		log.debug("Entering DELETE of Buchung, not available.");
	}

	@Override
	public Collection<Buchung> readAll() throws DAOException {
		log.debug("Entering READ ALL.");
		try {
			Collection<Buchung> collection = new ArrayList<Buchung>();
			ResultSet r = readAll.executeQuery();

			while (r.next()) {
				
					Buchung b = new Buchung();
					b.setId(r.getInt("buchung_id"));
					b.setKunde(r.getString("kunde"));
					
					readTermin.setInt(1, b.getId());
					ResultSet rt = readTermin.executeQuery();
					
					while (rt.next()) {
						Termin t = new Termin();
						t.setPferd(pferdDAO.read(rt.getInt("pferd_id")));
						t.setDatum(rt.getDate("datum"));
						t.setAusritt(rt.getBoolean("ausritt"));
						t.setStorno(rt.getBoolean("isStorno"));
						b.addTermin(t);
					}
					
					collection.add(b);
			}

			return collection;
		} catch (SQLException ex) {
			throw new DAOException();
		}
	}
	
	@Override
	public void storno(Buchung b, Termin t) throws DAOException{
		log.debug("Entering STORNO of Buchung, with parameters Buchung: " + b + ", Termin: " + t);
		try {
			if(b!=null && t!=null){
				updateStorno.setInt(1, b.getId());
				updateStorno.setDate(2, t.getDatum());
				updateStorno.executeUpdate();
			} else {
				throw new DAOException("Buchung oder Termin null");
			}
		} catch (SQLException ex) {
			throw new DAOException();
		}
	}
	

}
