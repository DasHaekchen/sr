package sepm.ws14.e0807136.persistence;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sepm.ws14.e0807136.dao.DAOException;
import sepm.ws14.e0807136.dao.PferdDAO;
import sepm.ws14.e0807136.domain.Pferd;
import sepm.ws14.e0807136.domain.Termin;

/**
 * Persistenz-Klasse des PferdDAOs
 * @author Saskia Reimerth, 0807136
 */
public class DBPferdDAO implements PferdDAO {

	private static final Logger log = LogManager.getLogger(DBPferdDAO.class);

	private PreparedStatement create;
	private PreparedStatement readAll;
	private PreparedStatement read;
	private PreparedStatement update;
	private PreparedStatement delete;
	private PreparedStatement getId;
	private PreparedStatement readTermine;

	/**
	 * Konstruktor des DBPferdDAOs. Erstellt die vorgefertigten SQL-Statements
	 * 
	 * @param c
	 *            die Verbindung zur Datenbank
	 * @throws DAOException
	 *             im Fehlerfall
	 */
	public DBPferdDAO(Connection c) throws DAOException {
		try {
			create = c
					.prepareStatement("INSERT INTO Pferd VALUES (DEFAULT, ?, ?, ?, ?, ?, TRUE);");
			readAll = c.prepareStatement("SELECT * FROM Pferd");
			read = c.prepareStatement("SELECT * FROM Pferd WHERE pferd_id = ?;");
			getId = c
					.prepareStatement("SELECT TOP 1 pferd_id FROM Pferd ORDER BY pferd_id DESC;");
			update = c
					.prepareStatement("UPDATE Pferd SET name = ?, jahre = ?, hoehe = ?, gewicht = ?, foto = ? WHERE pferd_id = ?;");
			delete = c
					.prepareStatement("UPDATE Pferd SET isActive = FALSE WHERE pferd_id = ?;");
			readTermine = c
					.prepareStatement("SELECT * FROM Termin WHERE pferd_id = ? AND isStorno = false ORDER BY datum;");
		} catch (SQLException ex) {
			log.error("Konstruktor mit Connection: " + c);
			throw new DAOException();
		}
	}

	@Override
	public void create(Pferd p) throws DAOException {
		log.debug("Entering CREATE with parameters: " + p);
		try {
			if (p == null) {
				throw new DAOException();
			}
			create.setString(1, p.getName());
			create.setInt(2, p.getJahre());
			create.setDouble(3, p.getHoehe());
			create.setInt(4, p.getGewicht());
			create.setString(5, p.getFoto());
			create.executeUpdate();

			ResultSet r = getId.executeQuery();
			if (!r.next()) {
				throw new DAOException();
			} else {
				p.setId(r.getInt("pferd_id"));
			}
		} catch (SQLException ex) {
			throw new DAOException();
		}
	}

	@Override
	public Pferd read(int i) throws DAOException {
		log.debug("Entering READ with parameter i: " + i);
		try {
			read.setInt(1, i);
			ResultSet r = read.executeQuery();
			if (!r.next()) {
				throw new DAOException();
			} else {
				Pferd p = new Pferd();
				p.setId(r.getInt("pferd_id"));
				p.setName(r.getString("name"));
				p.setJahre(r.getInt("jahre"));
				p.setHoehe(r.getDouble("hoehe"));
				p.setGewicht(r.getInt("gewicht"));
				p.setFoto(r.getString("foto"));
				p.setActive(r.getBoolean("isActive"));
				return p;
			}
		} catch (SQLException ex) {
			throw new DAOException();
		}
	}

	@Override
	public Collection<Pferd> readAll() throws DAOException {
		log.debug("Entering READALL");
		try {
			Collection<Pferd> collection = new ArrayList<Pferd>();
			ResultSet r = readAll.executeQuery();

			while (r.next()) {

				Pferd p = new Pferd();
				p.setId(r.getInt("pferd_id"));
				p.setName(r.getString("name"));
				p.setJahre(r.getInt("jahre"));
				p.setHoehe(r.getDouble("hoehe"));
				p.setGewicht(r.getInt("gewicht"));
				p.setFoto(r.getString("foto"));
				p.setActive(r.getBoolean("isActive"));
				collection.add(p);
			}

			return collection;
		} catch (SQLException ex) {
			throw new DAOException();
		}
	}

	@Override
	public void update(Pferd p) throws DAOException {
		log.debug("Entering UPDATE with parameters: " + p);
		try {
			if (p != null && read(p.getId()) != null) {
				update.setInt(6, p.getId());
				update.setString(1, p.getName());
				update.setInt(2, p.getJahre());
				update.setDouble(3, p.getHoehe());
				update.setInt(4, p.getGewicht());
				update.setString(5, p.getFoto());
				update.executeUpdate();
			} else {
				throw new DAOException("Pferd oder ID null");
			}
		} catch (SQLException ex) {
			throw new DAOException();
		}

	}

	@Override
	public void delete(Pferd p) throws DAOException {
		log.debug("Entering DELETE with parameters: " + p);
		try {
			if (p == null || p.getId() < 0) {
				throw new DAOException();
			}
			if (readTermine(p).isEmpty()) {
				delete.setInt(1, p.getId());
				delete.executeUpdate();
			} else {
				for (Termin t : readTermine(p)) {
					if (t.getDatum()
							.after(new Date(System.currentTimeMillis()))) {
						throw new DAOException(
								"Dieses Pferd hat noch anstehende Termine und kann daher nicht entfernt werden.");
					}
				}
				delete.setInt(1, p.getId());
				delete.executeUpdate();
			}
		} catch (SQLException ex) {
			throw new DAOException();
		}
	}

	@Override
	public Collection<Termin> readTermine(Pferd p) throws DAOException {
		log.debug("Entering READ TERMINE with parameters: " + p);
		Collection<Termin> termine = new ArrayList<Termin>();
		try {
			readTermine.setInt(1, p.getId());
			ResultSet r = readTermine.executeQuery();
			while (r.next()) {
				Termin t = new Termin();
				t.setPferd(p);
				t.setDatum(r.getDate("datum"));
				t.setAusritt(r.getBoolean("ausritt"));
				t.setStorno(r.getBoolean("isStorno"));
				termine.add(t);
			}
			return termine;
		} catch (SQLException ex) {
			throw new DAOException();

		}
	}

}
