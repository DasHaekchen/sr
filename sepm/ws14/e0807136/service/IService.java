package sepm.ws14.e0807136.service;

import java.sql.Date;
import java.util.Collection;

import sepm.ws14.e0807136.domain.Buchung;
import sepm.ws14.e0807136.domain.Pferd;
import sepm.ws14.e0807136.domain.Termin;

/**
 * Interface fuer Service-Implementationen
 * @author Saskia Reimerth, 0807136
 *
 */
public interface IService {

	//CRUD-Methoden: Weiterleitung an DAOs
	
	/**
	 * Erstellt ein neues Pferd - gibt es an die Persistenz-Schicht weiter
	 * @param p das neue Pferd
	 * @throws ServiceException
	 */
	public void createPferd(Pferd p) throws ServiceException;
	
	/**
	 * Bearbeitet ein Pferd
	 * @param p das ueberarbeitete Pferd
	 * @throws ServiceException
	 */
	public void updatePferd(Pferd p) throws ServiceException;
	
	/**
	 * Loescht ein Pferd
	 * @param p das zu loeschende Pferd
	 * @throws ServiceException
	 */
	public void deletePferd(Pferd p) throws ServiceException;
	
	/**
	 * Liest ein Pferd anhand seiner ID aus
	 * @param i die ID
	 * @return das gefundene Pferd
	 * @throws ServiceException 
	 */
	public Pferd readPferd(int i) throws ServiceException;
	
	/**
	 * Erstellt eine Liste aller in der Datenbank vorhandenen Pferde
	 * @return Eine Collection aller Pferde
	 * @throws ServiceException
	 */
	public Collection<Pferd> readAllPferde() throws ServiceException;
	
	/**
	 * Erstellt eine neue Buchung
	 * @param b
	 * @throws ServiceException
	 */
	public void createBuchung(Buchung b) throws ServiceException;
	
	/**
	 * Liest eine Buchung aus
	 * @param i die ID der Buchung
	 * @return die gefundene Buchung
	 * @throws ServiceException im Fehlerfall
	 */
	public Buchung readBuchung(int i) throws ServiceException;
	
	/**
	 * Ueberarbeitet eine Buchung
	 * @param b die Buchung
	 * @throws ServiceException im Fehlerfall
	 */
	public void updateBuchung(Buchung b) throws ServiceException;
	
	/**
	 * Liest alle Buchungen aus der Datenbank aus
	 * @return eine Collection mit allen Buchungen
	 * @throws ServiceException
	 */
	public Collection<Buchung> readAllBuchungen() throws ServiceException;
	
	
	
	// Servicespezifische Methoden
	
	/**
	 * Schraenkt das Ergebnis einer Suche mit den uebergebenen Parametern ein
	 * @param name Teilsequenz eines Namen, falls gesetzt, sonst null
	 * @param minjahre die minimalen Jahre des Pferdes, falls gesetzt, sonst 0
	 * @param maxjahre die maximalen Jahre des Pferdes, falls gesetzt, sonst 0
	 * @param minhoehe die minimale Hoehe des Pferdes, falls gesetzt, sonst 0
	 * @param maxhoehe die maximale Hoehe des Pferdes, falls gesetzt, sonst 0
	 * @param mingewicht das minimale Gewicht des Pferdes, falls gesetzt, sonst 0
	 * @param maxgewicht das maximale Gewicht des Pferdes, falls gesetzt, sonst 0
	 * @return eine Collection der Pferde, welche diesen Einschraenkungen entsprechen
	 * @throws ServiceException im Fehlerfall
	 */
	public Collection<Pferd> suchePferde(String name, int minjahre, int maxjahre, int minhoehe, int maxhoehe,
			int mingewicht, int maxgewicht) throws ServiceException;

	/**
	 * Ueberprueft, ob ein Pferd zu einem gegebenen Termin noch verfuegbar ist
	 * @param p das Pferd
	 * @param date der zu pruefende Termin
	 * @return true, wenn verfuegbar, false sonst
	 * @throws ServiceException im Fehlerfall
	 */
	public boolean checkAvailability(Pferd p, Date date) throws ServiceException;
	
	/**
	 * Liest alle Termine einer Buchung aus
	 * @param b die Buchung
	 * @return Collection der Termine
	 * @throws ServiceException im Fehlerfall
	 */
	public Collection<Termin> readAllTermine(Buchung b) throws ServiceException;
	
	/**
	 * Storniert einen Termin, falls dieser noch stornierbar ist
	 * @param b die Buchung
	 * @param t der zu stornierende Termin
	 * @throws ServiceException im Fehlerfall
	 */
	public void storniereTermin(Buchung b, Termin t) throws ServiceException;
	
	/**
	 * Gibt eine Bestaetigung der Buchung in Textform aus
	 * @param b die zu bestaetigende Buchung
	 * @return Textuelle Bestaetigung
	 * @throws ServiceException im Fehlerfall
	 */
	public String getBestaetigung(Buchung b) throws ServiceException;
	
	/**
	 * Schraenkt das Suchergebnis fuer Buchungen anhand eines Von-Bis Zeitraums ein
	 * @param vonDate Startdatum
	 * @param bisDate Enddatum
	 * @return Eine Collection mit den Ergebnissen
	 * @throws ServiceException im Fehlerfall
	 */
	public Collection<Buchung> sucheBuchungen(Date vonDate, Date bisDate) throws ServiceException;
	
	
	
}
