package sepm.ws14.e0807136.service;

import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import sepm.ws14.e0807136.dao.BuchungDAO;
import sepm.ws14.e0807136.dao.DAOException;
import sepm.ws14.e0807136.dao.PferdDAO;
import sepm.ws14.e0807136.domain.Buchung;
import sepm.ws14.e0807136.domain.Pferd;
import sepm.ws14.e0807136.domain.Termin;
import sepm.ws14.e0807136.persistence.DBBuchungDAO;
import sepm.ws14.e0807136.persistence.DBPferdDAO;

/**
 * Die Implementierung der Serviceschicht
 * @author Saskia Reimerth, 0807136
 */
public class SimpleService implements IService {

	private static final Logger log = LogManager.getLogger(SimpleService.class);
	private PferdDAO pferdDAO;
	private BuchungDAO buchungDAO;

	/**
	 * Konstruktor, welcher die Datenbankverbindung herstellt und die DAOs setzt.
	 * @throws ServiceException im Fehlerfall
	 */
	public SimpleService(Connection c) throws ServiceException {
		log.info("Konstruktor SimpleService");
		try {
			pferdDAO = new DBPferdDAO(c);
			buchungDAO = new DBBuchungDAO(c);
		} catch (DAOException e) {
			log.error("Konstruktor mit Connection: " + c);
			throw new ServiceException();
		}
	}

	@Override
	public void createPferd(Pferd p) throws ServiceException {
		checkPferd(p);
		try {
			pferdDAO.create(p);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Override
	public void updatePferd(Pferd p) throws ServiceException {
		log.debug("Entering UPDATE Pferd with parameters: " + p);
		checkPferd(p);
		if (p.getId() == -1)
			throw new IllegalArgumentException("Keine ID vorhanden");
		try {
			pferdDAO.update(p);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Override
	public void deletePferd(Pferd p) throws ServiceException {
		log.debug("Entering DELETE Pferd with parameters: " + p);
		if (p.getId() == -1)
			throw new IllegalArgumentException("Keine ID vorhanden");
		try {
			pferdDAO.delete(p);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Override
	public Pferd readPferd(int i) throws ServiceException {
		log.debug("Entering READ Pferd with parameter: " + i);
		try {
			return pferdDAO.read(i);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Override
	public Collection<Pferd> readAllPferde() throws ServiceException {
		log.debug("Entering READ ALL Pferde");
		try {
			return pferdDAO.readAll();
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Override
	public void createBuchung(Buchung b) throws ServiceException {
		log.debug("Entering CREATE Buchung with parameters: " + b);
		checkBuchung(b);
		try {
			buchungDAO.create(b);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Override
	public Buchung readBuchung(int i) throws ServiceException {
		log.debug("Entering READ Buchung with parameter: " + i);
		try {
			return buchungDAO.read(i);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Override
	public void updateBuchung(Buchung b) throws ServiceException {
		log.debug("Entering UPDATE Buchung with parameters: " + b);
		try {
			buchungDAO.update(b);
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Override
	public Collection<Buchung> readAllBuchungen() throws ServiceException {
		log.debug("Entering READ ALL Buchungen");
		try {
			return buchungDAO.readAll();
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Override
	public Collection<Pferd> suchePferde(String name, int minjahre,
			int maxjahre, int minhoehe, int maxhoehe, int mingewicht,
			int maxgewicht) throws ServiceException {
		log.debug("Entering SUCHE Pferde with parameters name: " + name
				+ ", minjahre: " + minjahre + ", maxjahre: " + maxjahre
				+ ", minhoehe; " + minhoehe + ", maxhoehe: " + maxhoehe
				+ ", mingewicht: " + mingewicht + ", maxgewicht: " + maxgewicht);
		
		Collection<Pferd> pferde = readAllPferde();
		Collection<Pferd> ergebnis = new ArrayList<Pferd>(pferde);

		for (Pferd p : pferde) {
			if (name != null) {
				if (!p.getName().contains(name)) {
					ergebnis.remove(p);
				}
			}
			if (minjahre != 0 || maxjahre != 0) {
				if (p.getJahre() < minjahre || p.getJahre() > maxjahre) {
					ergebnis.remove(p);
				}
			}
			if (minhoehe != 0 || maxhoehe != 0) {
				if (p.getHoehe() < minhoehe || p.getHoehe() > maxhoehe) {
					ergebnis.remove(p);
				}
			}
			if (mingewicht != 0 || maxgewicht != 0) {
				if (p.getGewicht() < mingewicht || p.getGewicht() > maxgewicht) {
					ergebnis.remove(p);
				}
			}

		}

		return ergebnis;

	}

	@Override
	public boolean checkAvailability(Pferd p, Date date)
			throws ServiceException {
		log.debug("Entering CHECK AVAILABILITY with parameters: " + p + ", " + date);
		
		if (p == null) {
			throw new ServiceException("Keine Pferd ausgewaehlt.");
		}
		if (date == null) {
			throw new ServiceException("Kein Termin ausgewaehlt.");
		}
		try {
			Collection<Termin> termine = pferdDAO.readTermine(p);
			for (Termin t : termine) {
				if (t.getDatum().equals(date)) {
					return false;
				}
			}
			return true;
		} catch (DAOException e) {
			throw new ServiceException();
		}
	}

	@Override
	public Collection<Termin> readAllTermine(Buchung b) throws ServiceException {
		log.debug("Entering READ ALL Termine with parameters: " + b);
		return b.getTermine();
	}

	@Override
	public void storniereTermin(Buchung b, Termin t) throws ServiceException {
		log.debug("Entering STORNIERE Termin with parameters: " + b + ", " + t);
		if (b == null) {
			throw new ServiceException("Keine Buchung ausgewaehlt.");
		}
		if (t == null) {
			throw new ServiceException("Kein Termin ausgewaehlt.");
		}

		checkStorno(t);
		t.setStorno(true);
		try {
			buchungDAO.storno(b, t);
		} catch (DAOException e) {
			t.setStorno(false);
			throw new ServiceException();
		}
	}

	@Override
	public String getBestaetigung(Buchung b) throws ServiceException {
		log.debug("Entering GET Bestaetigung with parameters: " + b);
		if (b == null) {
			throw new ServiceException("Keine Buchung ausgewaehlt.");
		}
		return b.toString();
	}

	@Override
	public Collection<Buchung> sucheBuchungen(Date vonDate, Date bisDate)
			throws ServiceException {
		log.debug("Entering SUCHE Buchung with parameters: " + vonDate + ", " + bisDate);
		
		Collection<Buchung> buchungen = readAllBuchungen();
		Collection<Buchung> ergebnis = new ArrayList<Buchung>(buchungen);

		for (Buchung b : buchungen) {
			if (b.getTermine().isEmpty()) {
				ergebnis.remove(b);
			} else {
				boolean hasTermin = false;
				for (Termin t : b.getTermine()) {
					if (t.getDatum().after(vonDate)
							&& t.getDatum().before(bisDate)) {
						hasTermin = true;
					}
				}
				if (!hasTermin) {
					ergebnis.remove(b);
				}

			}
		}

		return ergebnis;
	}

	/**
	 * Checkt ob ein Termin stornierbar ist
	 * @param t der Termin
	 * @return boolean, ob stornierbar
	 * @see http://tutorials.jenkov.com/java-date-time/index.html
	 */
	@SuppressWarnings("deprecation")
	private boolean checkStorno(Termin t) {
		log.debug("Entering CHECK Storno with parameters: " + t);
		java.sql.Date date1 = new java.sql.Date(System.currentTimeMillis());
		java.sql.Date date2 = t.getDatum();
		Calendar calendar = new GregorianCalendar();
		calendar.set(Calendar.YEAR, date1.getYear());
		calendar.set(Calendar.MONTH, date1.getMonth());
		calendar.set(Calendar.DAY_OF_MONTH, date1.getDate());
		calendar.add(Calendar.DAY_OF_MONTH, 14);
		date1 = (Date) calendar.getTime();
		return date1.before(date2);
	}

	/**
	 * Ueberprueft, ob die Parameter eines Pferdes richtig gesetzt sind
	 * @param p das Pferd
	 * @throws IllegalArgumentException falls ein Parameter nicht korrekt ist
	 */
	private static void checkPferd(Pferd p) throws IllegalArgumentException {
		log.debug("Entering CHECK Pferd with parameters: " + p);
		if (p == null)
			throw new IllegalArgumentException("Kein Pferd vorhanden");
		if (p.getName() == null)
			throw new IllegalArgumentException(
					"Dieses Pferd besitzt keinen Namen");
		if (p.getName().length() > 30)
			throw new IllegalArgumentException(
					"Der Name des Pferdes ist zu lang");
		if (p.getJahre() < 0)
			throw new IllegalArgumentException(
					"Dieses Pferd ist unter 0 Jahre alt");
		if (p.getHoehe() <= 0)
			throw new IllegalArgumentException(
					"Dieses Pferd ist unter 0 cm hoch");
		if (p.getGewicht() <= 0)
			throw new IllegalArgumentException(
					"Dieses Pferd ist unter 0 kg schwer");
		if (p.getFoto().length() > 200)
			throw new IllegalArgumentException("Der Pfad des Fotos ist zu lang");
	}

	/**
	 * Ueberprueft, ob die Parameter einer Buchung richtig gesetzt sind
	 * @param b die Buchung
	 * @throws IllegalArgumentException falls ein Parameter nicht korrekt ist
	 */
	private static void checkBuchung(Buchung b) throws IllegalArgumentException {
		log.debug("Entering CHECK Buchung with parameters: " + b);
		if (b.getKunde() == null)
			throw new IllegalArgumentException("Kein Kunde vorhanden.");
		if (b.getKunde().length() > 30)
			throw new IllegalArgumentException("Der Kundenname ist zu lang.");
		if (b.getTermine().isEmpty())
			throw new IllegalArgumentException("Keine Termine vorhanden.");
	}

}
