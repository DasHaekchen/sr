package sepm.ws14.e0807136.service;

/**
 * Spezielle Service-Exception
 * @author Saskia Reimerth, 0807136
 */
public class ServiceException extends Exception{

	/**
	 * Default UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Weitergabe ohne Nachricht
	 */
	public ServiceException(){
		super();
	}
	
	/**
	 * Weitergabe mit Nachricht
	 * @param ex die Meldung
	 */
	public ServiceException(String ex){
		super(ex);
	}
}
