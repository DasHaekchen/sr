package security;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * TCP Channel 
 */
public class TCPChannel implements Channel {

	private BufferedReader reader;
	private PrintWriter writer;

	public TCPChannel(Socket socket) {
		try {
			reader = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			writer = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException e) {
			//world's gonna end
		}
	}

	@Override
	public void send(String msg) {
		writer.println(msg);
	}

	@Override
	public String recieve() {
		try {
			return reader.readLine();
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public void sendByte(byte[] msg) {
		//not needed
	}

	@Override
	public byte[] recieveByte() {
		//not needed
		return null;
	}

}
