package security;

/**
 * Interface for channels
 */
public interface Channel {

	/**
	 * Sends a message
	 * @param msg message to send
	 */
	public void send(String msg);
	
	/**
	 * Recieves a message
	 * @return the recieved message
	 */
	public String recieve();

	/**
	 * Sends a byte array as a message 
	 * @param msg bytes to send
	 */
	public void sendByte(byte[] msg);

	/**
	 * Recieves a message and returns it as a byte array
	 * @return the message as bytes
	 */
	public byte[] recieveByte();
	
}
