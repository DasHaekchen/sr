package security;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.bouncycastle.util.encoders.Base64;

import util.Keys;

/**
 * Secure Authentication (cloud sided)
 * Handles new connections
 */
public class CloudSecureAuthentication {

	private File clientKeyDir;
	private File cloudKeyDir;
	private PrivateKey cloudPrivateKey;
	private String user;
	private byte[] challenge;
	private Cipher cipherEncrypt;
	private Cipher cipherDecrypt;
	final private int KEYSIZE=256;

	public CloudSecureAuthentication(String clientKeyDir, String cloudKeyDir)
			throws SecurityException {
		this.clientKeyDir = new File(clientKeyDir);
		this.cloudKeyDir = new File(cloudKeyDir);

		String path = this.cloudKeyDir.getPath();
		File cloudKeyFile = new File(path);
		try {
			cloudPrivateKey = Keys.readPrivatePEM(cloudKeyFile);
		} catch (IOException e) {
			throw new SecurityException(
					"Could not read controller's PrivateKey.");
		}

		try {
			cipherEncrypt = Cipher
					.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
			cipherDecrypt = Cipher
					.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			throw new SecurityException("Could not get cipher's instance.");
		}
		try {
			cipherDecrypt.init(Cipher.DECRYPT_MODE, cloudPrivateKey);
		} catch (InvalidKeyException e) {
			throw new SecurityException("Could not initialize cipher.");
		}

	}

	/**
	 * Checks and authenticates the client via challenges
	 * @param channel the used channel
	 * @return AESWrapper containing Channel, username and a message
	 */
	public AESWrapper authenticateClient(Channel channel)
			throws SecurityException {
		SecureRandom secureRandom = new SecureRandom();
		final byte[] number = new byte[32];
		secureRandom.nextBytes(number);
		challenge = Base64.encode(number);

		byte[] firstMsg = channel.recieveByte();
		if (firstMsg == null) {
			throw new SecurityException("Error while reading from the channel.");
		}

		// RECIEVE FIRST MSG !authenticate <username> <client-challenge>
		byte[] message;
		try {
			message = cipherDecrypt.doFinal(firstMsg);
		} catch (IllegalBlockSizeException | BadPaddingException e2) {
			throw new SecurityException("Error while decrypting.");
		}

		String[] content = (new String(message)).split(" ");
		user = content[1];
		String path = clientKeyDir.getPath() + File.separator + user
				+ ".pub.pem";
		File clientPublicKeyFile = new File(path);
		PublicKey clientPublicKey = null;

		try {
			clientPublicKey = Keys.readPublicPEM(clientPublicKeyFile);
		} catch (IOException e2) {
			throw new SecurityException(
					"Could not read the client's PublicKey.");
		}

		try {
			cipherEncrypt.init(Cipher.ENCRYPT_MODE, clientPublicKey);
		} catch (InvalidKeyException e2) {
			throw new SecurityException("Could not initialize cipher.");
		}

		// SEND SECOND MSG !ok <client-challenge> <controller-challenge>
		// <secret-key> <iv-parameter>
		String second = "!ok " + content[2] + " " + new String(challenge) + " ";

		KeyGenerator generator = null;

		try {
			generator = KeyGenerator.getInstance("AES");
		} catch (NoSuchAlgorithmException e2) {
			throw new SecurityException(
					"Could not get the KeyGenerator's instance.");
		}

		// KEYSIZE is in bits
		generator.init(KEYSIZE);
		SecretKey secretKey = generator.generateKey();
		byte[] secretKey64 = Base64.encode(secretKey.getEncoded());

		SecureRandom secureRandom2 = new SecureRandom();
		final byte[] iv = new byte[16];
		secureRandom2.nextBytes(iv);
		byte[] ivParameter = Base64.encode(iv);
		second += new String(secretKey64) + " " + new String(ivParameter);
		byte[] secondEncr;
		try {
			secondEncr = cipherEncrypt.doFinal(second.getBytes());
		} catch (IllegalBlockSizeException | BadPaddingException e1) {
			throw new SecurityException("Error while encrypting.");
		}
		channel.sendByte(secondEncr);

		// Create AESChannel
		Channel aesChannel = new AESChannel(secretKey, iv, channel);
		AESWrapper wrapper = new AESWrapper(aesChannel, user, "Success");

		// RECIEVE THIRD MSG <controller-challenge>
		String thirdMsg = aesChannel.recieve();
		if (thirdMsg == null) {
			throw new SecurityException("Error while reading from the channel.");
		}

		if (thirdMsg.equals(new String(challenge))) {
			return wrapper;
		} else {
			throw new SecurityException("Challenges do not match.");
		}
	}
}
