package security;

import org.bouncycastle.util.encoders.Base64;

/**
 * Base64-encrypted secure channel
 */
public class Base64Channel implements Channel {

	private Channel channel;

	public Base64Channel(Channel c) {
		this.channel = c;
	}

	@Override
	public void send(String msg) {
		byte[] message = Base64.encode(msg.getBytes());
		channel.send(new String(message));
	}

	@Override
	public String recieve() {
		String msg = channel.recieve();
		if(msg == null){
			return null;
		}
		byte[] decoded = Base64.decode(msg);
		return new String(decoded);
	}
	
	@Override
	public byte[] recieveByte(){
		String msg = channel.recieve();
		if(msg == null){
			return null;
		}
		return Base64.decode(msg);
	}
	
	@Override
	public void sendByte(byte[] msg){
		byte[] message = Base64.encode(msg);
		channel.send(new String(message));
	}


}
