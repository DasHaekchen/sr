package security;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

/**
 * AES-encrypted secure channel
 */
public class AESChannel implements Channel{

	private Cipher encryptCipher;
	private Cipher decryptCipher;
	
	private Channel channel;
	
	public AESChannel(SecretKey secret, byte[] iv, Channel c) throws SecurityException{
		channel = c;
		
		try {
			encryptCipher = Cipher.getInstance("AES/CTR/NoPadding");
			decryptCipher = Cipher.getInstance("AES/CTR/NoPadding");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			throw new SecurityException("Could not get instance of AES Cipher.");
		}
		
		try {
			encryptCipher.init(Cipher.ENCRYPT_MODE, secret, new IvParameterSpec(iv));
			decryptCipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(iv));
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			throw new SecurityException("Could not initialize AES Cipher.");
		}
	}
	
	@Override
	public void send(String msg) {
		try {
			byte[] encrypted = encryptCipher.doFinal(msg.getBytes());
			channel.sendByte(encrypted);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			//do nothing
		}
	}

	@Override
	public String recieve() {
		byte[] msg = channel.recieveByte();
		try {
			byte[] decrypted = decryptCipher.doFinal(msg);
			return new String(decrypted);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			return null;
		}
	}

	@Override
	public void sendByte(byte[] msg) {
		// unused
		
	}

	@Override
	public byte[] recieveByte() {
		// unused
		return null;
	}

	// Setter
	public void setChannel(Channel c){
		this.channel = c;
	}

}
