package security;

/**
 * Security Exception
 */
public class SecurityException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public SecurityException(){}
	
	public SecurityException(String s){
		super(s);
	}
	
}
