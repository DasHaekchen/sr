package security;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.util.encoders.Base64;

import util.Keys;

/**
 * Secure Authentication (client sided)
 * Handles new connections
 */
public class ClientSecureAuthentication {

	private PublicKey cloudPublicKey = null;
	private PrivateKey clientPrivateKey = null;
	private byte[] challenge;
	private File cloudKeyDir;
	private File clientKeyDir;
	private String username;
	private Cipher cipherEncrypt;
	private Cipher cipherDecrypt;

	public ClientSecureAuthentication(String username, String cloudKeyDir,
			String clientKeyDir) throws SecurityException {
		this.username = username;
		this.cloudKeyDir = new File(cloudKeyDir);
		this.clientKeyDir = new File(clientKeyDir);

		SecureRandom secureRandom = new SecureRandom();
		final byte[] number = new byte[32];
		secureRandom.nextBytes(number);
		challenge = Base64.encode(number);

		String path = this.cloudKeyDir.getPath();
		File cloudKeyFile = new File(path);

		try {
			cloudPublicKey = Keys.readPublicPEM(cloudKeyFile);
		} catch (Exception e) {
			throw new SecurityException(
					"Could not read the controller's PublicKey.");
		}

		String path2 = this.clientKeyDir.getPath() + File.separator + username
				+ ".pem";
		File clientKeyFile = new File(path2);
		try {
			clientPrivateKey = Keys.readPrivatePEM(clientKeyFile);
		} catch (IOException e) {
			throw new SecurityException(
					"Could not read the client's PrivateKey.");
		}

		try {
			cipherEncrypt = Cipher
					.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
			cipherDecrypt = Cipher
					.getInstance("RSA/NONE/OAEPWithSHA256AndMGF1Padding");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			throw new SecurityException("Could not get cipher's instance.");
		}
		try {
			cipherEncrypt.init(Cipher.ENCRYPT_MODE, cloudPublicKey);
			cipherDecrypt.init(Cipher.DECRYPT_MODE, clientPrivateKey);
		} catch (InvalidKeyException e) {
			throw new SecurityException("Could not initialize cipher.");
		}

	}

	/**
	 * Authenticate the cloud
	 * @param channel the used channel
	 * @return AESWrapper containing Channel, username and a message
	 */
	public AESWrapper authenticateCloud(Channel channel) throws SecurityException {

		// SEND FIRST MSG !authenticate <username> <client-challenge>
		String msg = "!authenticate " + username + " ";
		byte[] one = msg.getBytes();
		byte[] combined = new byte[one.length + challenge.length];
		for (int i = 0; i < combined.length; ++i) {
			combined[i] = i < one.length ? one[i] : challenge[i - one.length];
		}
		byte[] msgEncrypt;
		try {
			msgEncrypt = cipherEncrypt.doFinal(combined);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new SecurityException("Error while encrypting.");
		}
		channel.sendByte(msgEncrypt);

		// RECIEVE SECOND MSG !ok <client-challenge> <controller-challenge>
		// <secret-key> <iv-parameter>
		byte[] secondMsg = channel.recieveByte();
		if(secondMsg == null){
			throw new SecurityException("Error while reading from socket.");
		}

		
		byte[] secondDecrypt;
		try {
			secondDecrypt = cipherDecrypt.doFinal(secondMsg);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			throw new SecurityException("Error while decrypting.");
		}
		String[] content = (new String(secondDecrypt)).split(" ");
		if (!content[1].equals(new String(challenge))) {
			throw new SecurityException("Challenges do not match.");
		}

		// READ SecretKey und IV-Parameter
		String cloudChallenge = content[2];
		
		byte[] secret = Base64.decode(content[3].getBytes());
		SecretKey secretKey = new SecretKeySpec(secret, "AES");
		byte[] iv = Base64.decode(content[4].getBytes());
		AESChannel aesChannel = new AESChannel(secretKey, iv, channel);

		// SEND THIRD MSG <controller-challenge>
		aesChannel.send(cloudChallenge);

		return new AESWrapper(aesChannel, username, "Successfully authenticated.");
	}

}