package security;

/**
 * Wrapper-class containing a secure channel, username and a message
 */
public class AESWrapper {

	private Channel channel;
	
	private String username;
	
	private String msg;
	
	/**
	 * Constructor
	 */
	public AESWrapper(Channel c, String user, String msg){
		this.channel = c;
		this.username = user;
		this.setMsg(msg);
	}

	/**
	 * Getter and setter
	 */
	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
