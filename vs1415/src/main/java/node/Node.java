package node;


import util.Config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import model.ComputationRequestInfo;


public class Node implements INodeCli, Runnable {

	private String componentName;
	private Config config;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	private ListenerThread listen;

	private UdpHandler udp;
	//static private Boolean run;
	public int rmin;
	public int ressoucre;
	public Boolean status;

	private ServerSocket serverSocket;

	private ArrayList<ComputationRequestInfo> loglist;
	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public Node(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;

	}

	@Override
	public void run() {

		try {
			//loglist = new ArrayList<ComputationRequestInfo>();
			ressoucre=0;
			rmin=config.getInt("node.rmin");

			serverSocket = new ServerSocket(config.getInt("tcp.port"));
			listen=new ListenerThread(serverSocket,config.getString("log.dir"),componentName,this);
			listen.start();

			udp=new UdpHandler( config,this);
			udp.start();


		} catch (IOException e) {

		}

		//Node Interaktion
		BufferedReader reader = new BufferedReader(new InputStreamReader(userRequestStream));

		String input="";
		boolean run=true;

		while(run){
			try {
				input=reader.readLine();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				userResponseStream.println("Error in NODE");
				run=false;
			}
			try {
				switch ( input )
				{
				case "!exit":
					run=false;

					exit();
					break;
				case "!resources":

					userResponseStream.println(ressoucre);
					break;
				default:
					userResponseStream.println("Unknown Command " + input);
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	}

	@Override
	public String exit() throws IOException {
		// TODO Auto-generated method stub

		if (serverSocket != null)
			try {
				serverSocket.close();
				udp.stop();
			} catch (IOException e) {
				// Ignored because we cannot handle it
			}

		return null;
	}

	@Override
	public String history(int numberOfRequests) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link Node} component,
	 *            which also represents the name of the configuration
	 */
	public static void main(String[] args) {
		Node node = new Node(args[0], new Config(args[0]), System.in,
				System.out);
		node.run();
	
	}

	// --- Commands needed for Lab 2. Please note that you do not have to
	// implement them for the first submission. ---

	@Override
	public String resources() throws IOException {
		
		return null;
	}

	public String getName(){
		return componentName;
	}

}

class ListenerThread extends Thread implements Runnable {

	private ServerSocket serverSocket;
	private String log;
	private String name;
	private Node node;

	public ListenerThread(ServerSocket serverSocket,String log,String name,Node node) {
		this.serverSocket = serverSocket;
		this.log=log;
		this.name=name;
		this.node=node;
	}

	public void run()
	{
		System.out.println("NODE-worker ready for work.");
		while (true) {
			// wait for Client to connect
			try {
				//pool.execute(new Handler(serverSocket.accept(),config));
				check(serverSocket.accept());

			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("NODE-worker shutdown.");
				break;
				//pool.shutdownNow();	
			}

		}

	}

	public void check(Socket socket){

		String input = null;
		try {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));

			PrintWriter writer = new PrintWriter(socket.getOutputStream(),
					true);
			input = reader.readLine();

			String[] parts = input.split(" ");

			switch ( parts[0])
			{
			case "!share":

				if(node.rmin<=Integer.parseInt(parts[1])){
					writer.println("!ok");
				}else{
					writer.println("!nok");
				}

				break;
			case "!commit":
				node.ressoucre=Integer.parseInt(parts[1]);
				break;
			case "!rollback": //Nothing xD
				break;	  

			default:
				compute(socket,reader,writer,input);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public void compute(Socket socket,BufferedReader reader,PrintWriter writer,String request) {

		// read and service request on socket
		try {
			//BufferedReader reader = new BufferedReader(
			//	new InputStreamReader(socket.getInputStream()));

			//	PrintWriter writer = new PrintWriter(socket.getOutputStream(),
			//		true);

			//String request; // 10 + 12
			//request = reader.readLine();


			String[] parts = request.split(" ");
			boolean div0=false;
			int resultz=0;
			int z1= Integer.parseInt(parts[0]);
			int z2= Integer.parseInt(parts[2]);

			switch ( parts[1] )
			{
			case "+":
				resultz=z1+z2;
				break;
			case "-":
				resultz=z1-z2;
				break;
			case "*":
				resultz=z1*z2;
				break;
			case "/":
				if (z2!=0){
					resultz=z1/z2;
				} else {div0=true; }
				break;
			}


			String result=  String.valueOf(resultz);

			if(div0){result="Error: division by 0";}
			writer.println(result);

			//write logFile
			loging(request,result,socket);
			
			socket.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	void loging(String text,String result, Socket s)
	{
		Writer fw = null;
		ObjectOutputStream objectout=null;
		try
		{
			Date time = Calendar.getInstance().getTime();
			String logname=inDateFormatHolder.get().format(time);

			String path=log+"/"+logname+"_"+name+".log";

			// final File parent_directory = file.getParentFile();
			File file = new File(path);
			File parent_directory = file.getParentFile();

			if (null != parent_directory)
			{
				parent_directory.mkdirs();
			}

			fw = new FileWriter( path);
			fw.write( text+ System.getProperty("line.separator")+result );
			// fw.append( System.getProperty("line.separator") ); // e.g. "\n"
			
			/*
			 * Lab2 Task 4 
			 * */
			
			objectout = new ObjectOutputStream(s.getOutputStream());
			objectout.writeObject(new ComputationRequestInfo(logname,node,text, result));
			
		}
		catch ( IOException e ) {
			System.err.println( "Konnte Datei nicht erstellen" );
		}
		finally {
			if ( fw != null )
				try { fw.close(); objectout.close(); } catch ( IOException e ) { e.printStackTrace(); }
		}

	}

	private static ThreadLocal<SimpleDateFormat> inDateFormatHolder = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyyMMdd_HHmmss.SSS");
		}
	};

}

class UdpHandler extends Thread implements Runnable {
	private Config config;
	private Node node;
	private  ExecutorService pool;

	UdpHandler(Config config,Node node) { 
		this.config=config;
		this.node=node;
		pool = Executors.newCachedThreadPool();
	}

	public void run() {

		DatagramSocket socket = null;
		Boolean init = false;
		byte[] buffer;
		DatagramPacket packet = null;
		int port=config.getInt("controller.udp.port");
		int tcpport=config.getInt("tcp.port");
		String addr=config.getString("controller.host");
		int timeout=config.getInt("node.alive");

		//lab2
		node.status=false;
		try {
			socket = new DatagramSocket();
			buffer = new byte[1024];

			String data="!hello";
			buffer = data.getBytes();

			packet = new DatagramPacket(buffer, buffer.length,
					InetAddress.getByName(addr),port);
			socket.send(packet);

		} catch (UnknownHostException e2) {
			socket.close();
			e2.printStackTrace();

		} catch (IOException e) {
			socket.close();
			e.printStackTrace();

		}finally{}


		byte[] buffer2 = new byte[1024];
		DatagramPacket packet2 = new DatagramPacket(buffer2, buffer2.length);

		// wait for incoming packets from client
		try {
			socket.receive(packet2);
		} catch (IOException e2) {

			e2.printStackTrace();
		}

		// get the data from the packet
		String request = new String(packet2.getData()).trim();
		String[] parts = request.split("#");

		Boolean state=false;
		int rmax=0;
		if (parts[0].startsWith("!init"))
		{

			if(parts.length==2) 
			{// first Node
				node.ressoucre=Integer.parseInt(parts[1]);	
				if (node.ressoucre>=node.rmin){init=true;}
			}else
			{
				// ask all nodes
				rmax=Integer.parseInt(parts[parts.length-1])/(parts.length-1);
				if(rmax>=node.rmin)
				{
					for(int i=0;i<parts.length-2;i++)
					{
						pool.execute(new NodeHandler(parts[i+1],rmax,node,(parts.length-2)));
					}

					try {
						pool.shutdown();
						state=pool.awaitTermination(3000,TimeUnit.MILLISECONDS );
					} catch (InterruptedException e) {

						e.printStackTrace();
					}
				}else{}//TODO
			}

		}

		if (node.status)
		{
			if (state)
			{
				init=true;	
				node.ressoucre=rmax;	
			}
		}
		if(init==false){
			System.out.println("Not enough ressouces aviable");
		}else{
			System.out.println("Node online "+ node.ressoucre +"/"+node.rmin);
		}

		socket.close();
		pool.shutdownNow();
		//

		while(init){

			try {

				socket = new DatagramSocket();
				buffer = new byte[1024];

				String data="!alive "+ tcpport+" "+ config.getString("node.operators");

				buffer = data.getBytes();

				packet = new DatagramPacket(buffer, buffer.length,
						InetAddress.getByName(addr),port);

				socket.send(packet);

			} catch (IOException e1) {

			}
			socket.close();

			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {

			}
		}

	}
} 

class NodeHandler extends Thread implements Runnable {
	private String host;

	private int rmax;
	private Node node;
	private int anz;
	/*	
	 public static final ThreadLocal<Integer> ok =
             new ThreadLocal<Integer>()
{
@Override protected Integer initialValue() { return 0; }
};

public static final ThreadLocal<Integer> nok =
new ThreadLocal<Integer>()
{
@Override protected Integer initialValue() { return 0; }
};
	 */
	private static int ok=0;
	private static int nok=0;

	NodeHandler(String host,int rmax,Node node,int anz) { 

		this.host=host;
		this.rmax=rmax;
		this.node=node;
		this.anz=anz;
	}

	public void run() {

		String[] parts = host.split(":");
		Socket socket = null;
		String response ="";
		try {
			socket = new Socket(parts[0],Integer.parseInt(parts[1]));
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader serverReader = null;
		PrintWriter serverWriter = null;
		try {
			serverWriter = new PrintWriter(
					socket.getOutputStream(), true);
			serverReader = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));

			serverWriter.println("!share "+rmax);


			response = serverReader.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		//	int test= ok.get();
		if(response.startsWith("!ok"))
		{
			// ok.set( ok.get() + 1 );
			okinc();

		}else
		{

			// nok.set( nok.get() + 1 );
			nokinc();
		}

		Boolean rollback=false;
		while(true)
		{
			//test= ok.get();
			if(nokget()>0)
			{
				rollback=true;
				break;
			}

			if(okget()==anz)
			{

				break;
			}

			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				break;
			}
		}

		//send back
		if(rollback){
			serverWriter.println("!rollback");
		}else{
			serverWriter.println("!commit "+rmax);
			node.status=true;
		}

		//nok.remove();
		//ok.remove();

	}

	private synchronized void okinc() {
		ok+=1;
	}

	private synchronized int okget() {
		return  ok;
	}
	private synchronized void nokinc() {
		nok+=1;
	}
	private synchronized int nokget() {
		return  nok;
	}

}

