package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import security.AESWrapper;
import security.Base64Channel;
import security.Channel;
import security.ClientSecureAuthentication;
import security.SecurityException;
import security.TCPChannel;
import util.Config;
import util.SecurityUtils;

public class Client implements IClientCli, Runnable {

	private String componentName;
	private Config config;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;
	private Socket socket;

	private Boolean run = false;
	private Boolean login;

	private Channel channel;

	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public Client(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;

		SecurityUtils.registerBouncyCastle();
		
	}

	@Override
	public void run() {

		run = true;
		login = false;
		BufferedReader userInputReader = null;
		try {
			socket = new Socket(config.getString("controller.host"),
					config.getInt("controller.tcp.port"));
			this.channel = new Base64Channel(new TCPChannel(socket));
			
		} catch (UnknownHostException e) {
			userResponseStream.println("Cannot connect to host: " + e.getMessage());
			run = false;
		} catch (IOException e) {
			//Do nothing
		}

		// create the client input reader from command line
		userInputReader = new BufferedReader(new InputStreamReader(
				userRequestStream));

		userResponseStream.println("Connected with Server! Enter command.");

		String input = "";
		String[] parts;
		while (run) {
			if (socket.isClosed())
				try {
					exit();
					break;
				} catch (IOException e2) {
				}

			try {
				input = userInputReader.readLine();
				if (login == false) {

					if (input.startsWith("!exit")) {
						exit();
						break;
					}

					if (input.startsWith("!authenticate")) {
						parts = input.split(" ");
						if (parts.length == 2) {
							userResponseStream.println(authenticate(parts[1]));
						} else {
							userResponseStream
									.println("Usage: !authenticate <user>");
						}
					} else {
						userResponseStream
								.println("Please authenticate first: !authenticate %user");
						continue;
					}

				} else { // user has logged in
					parts = input.split(" ");
					switch (parts[0]) {
					case "!login":
						userResponseStream
								.println("You are already logged in!");
						break;
					case "!credits":
						userResponseStream.println(credits());
						break;
					case "!buy":
						if (parts.length == 2) {
							userResponseStream.println(buy(Long
									.parseLong(parts[1])));
						}
						break;
					case "!list":
						userResponseStream.println(list());
						break;
					case "!logout":
						userResponseStream.println(logout());
						break;
					case "!compute":
						userResponseStream.println(compute(input.substring(9)));
						break;
					case "!exit":
						logout();
						exit();
						break;
					default:
						userResponseStream.println("Wrong Command! Client");
					}

				}
			} catch (IOException e) {
				try {
					socket.close();
				} catch (IOException e1) {
					// unhandled
				}
				run = false;
				userResponseStream.println(e.getClass().getSimpleName() + ": "
						+ e.getMessage());
			}

		}

		try {
			if ((userInputReader != null)) {
				userInputReader.close();
			}
		} catch (IOException e) {
			// unimportant
		}

	}

	@Override
	public String login(String username, String password) throws IOException {

		channel.send("!login " + username + " " + password);
		// read server response and write it to console
		String response = channel.recieve();

		if (response == null) {
			run = false;
			exit();
			return "Disconnected from Server";
		}

		if (response.startsWith("success")) {
			login = true;
			response = "Successfully logged in.";
		} else {
			response = "Wrong username or password.";
		}

		return response;
	}

	@Override
	public String logout() throws IOException {

		channel.send("!logout");
		// read server response and write it to console
		String response = channel.recieve();
		
		if (response == null) {
			exit();
			return "Disconnected from Server";
		}

		if (response.startsWith("success")) {
			login = false;
			this.channel = new Base64Channel(new TCPChannel(socket));
			response = "logged out.";
		} else {
			response = "not possible";
		}

		return response;
	}

	@Override
	public String credits() throws IOException {

		channel.send("!credits");
		// read server response and write it to console
		String response = channel.recieve();

		if (response == null) {
			exit();
			return "Disconnected from Server";
		}

		return response;
	}

	@Override
	public String buy(long credits) throws IOException {

		channel.send("!buy " + credits);
		// read server response and write it to console
		String response = channel.recieve();

		if (response == null) {
			exit();
			return "Disconnected from Server";
		}

		return response;
	}

	@Override
	public String list() throws IOException {

		channel.send("!list");
		// read server response and write it to console
		String response = channel.recieve();

		if (response == null) {
			exit();
			return "Disconnected from Server";
		}

		return response;
	}

	@Override
	public String compute(String term) throws IOException {

		channel.send("!compute " + term);
		// read server response and write it to console
		String response = channel.recieve();

		if (response == null) {
			exit();
			return "Disconnected from Server";
		}

		return response;

	}

	@Override
	public String exit() throws IOException {
		try {
			run = false;
			socket.close();

		} catch (IOException e) {
			// Ignored because we cannot handle it
		}
		return null;
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link Client} component
	 */
	public static void main(String[] args) {
		Client client = new Client(args[0], new Config("client"), System.in,
				System.out);
		client.run();
	}

	@Override
	public String authenticate(String username){
		String auth = "Failure authenticating";
		ClientSecureAuthentication secure;
		try {
			secure = new ClientSecureAuthentication(username,
					config.getString("controller.key"),
					config.getString("keys.dir"));
			AESWrapper wrapper = secure.authenticateCloud(this.channel);
			auth = wrapper.getMsg();
			this.channel = wrapper.getChannel();
			login = true;
		} catch (SecurityException e1) {
			auth = e1.getMessage();
		}
		return auth;
	}

}