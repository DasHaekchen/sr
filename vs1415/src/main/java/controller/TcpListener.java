package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sql.rowset.serial.SerialException;

import security.AESWrapper;
import security.Base64Channel;
import security.Channel;
import security.SecurityException;
import security.TCPChannel;
import model.ComputationRequestInfo;

public class TcpListener extends Thread implements Runnable {

	private ServerSocket serverSocket;
	private ExecutorService pool;
	private CloudController controller;

	public TcpListener(ServerSocket serverSocket, CloudController controller) {
		this.serverSocket = serverSocket;
		pool = Executors.newCachedThreadPool();
		this.controller = controller;

	}

	public void getNodes() {
		System.out.println("Nodes");
	}

	public void run() {
		System.out.println("TCP-worker ready for work.");
		while (true) {
			// wait for Client to connect
			try {
				pool.execute(new Handler(serverSocket.accept(), controller));

			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("TCP-worker shutdown.");
				pool.shutdownNow();
				try {
					serverSocket.close();
					break;
				} catch (IOException e1) {

					// System.out.println("TCP sTrigger.");
					// e1.printStackTrace();
				}
			}

		}
	}
}

class Handler implements Runnable {
	private Socket socket;
	private CloudController controller;
	private int maxN = 50;
	private Channel channel;

	Handler(Socket socket, CloudController controller) {
		this.socket = socket;
		this.controller = controller;
		this.channel = new Base64Channel(new TCPChannel(socket));
	}

	public void run() {
		// read and service request on socket

		// What to do
		String name = ""; // Whos connection is it?
		while (!(socket.isClosed())) {
			String input = "";

			try {

				if (name.length() == 0) {

					try {
						AESWrapper wrap = controller.secureAuth
								.authenticateClient(channel);
						this.channel = wrap.getChannel();
						name = wrap.getUsername();
						controller.onlineUsers.put(wrap.getUsername(), true);
					} catch (SecurityException e) {
						// Do nothing
					}

				} else {

					String response = "";
					input = channel.recieve();
					// System.out.println("input="+input);
					if (input == null || controller.run == false) {
						socket.shutdownOutput();
						socket.shutdownInput();
						socket.close();
						break;
					}
					
					String[] parts = input.split(" ");
					int usercr = 0;
					switch (parts[0]) {
					// case "!login":
					// String userpw = controller.userPass.get(parts[1]);
					// if (userpw == null) {
					// } else if (parts[2].equals(userpw)) {
					// controller.onlineUsers.put(parts[1], true);
					// name = parts[1];
					// response = "success";
					// }
					//
					// break;
					case "!credits":
						usercr = controller.userData.get(name);
						response = String.valueOf(usercr);
						break;
					case "!buy":
						usercr = controller.userData.get(name);
						usercr += Integer.parseInt(parts[1]);
						controller.userData.put(name, usercr);
						response = "You now have " + String.valueOf(usercr)
								+ " credits.";

						break;
					case "!list":
						response = list();

						break;
					case "!logout":
						controller.onlineUsers.put(name, false);
						name = "";
						response = "success";
						break;
					case "!compute":
						response = compute(input, name);
						break;
					default:
						// do nothing
					}

					// write back
					// serverWriter.println(response);
					channel.send(response);

					if (parts[0].equals("!logout")
							&& response.equals("success")) {
						this.channel = new Base64Channel(new TCPChannel(socket));
					}
				}

			} catch (IOException e) {
				// e.printStackTrace();
				try {
					socket.close();
					break;
				} catch (IOException e1) {
					// forget
				}
			}

		}
	}

	public String list() {

		String str = "";
		String result = "";

		Iterator<Entry<String, Boolean>> it = controller.aliveMap.entrySet()
				.iterator();
		while (it.hasNext()) {
			Entry<String, Boolean> pairs = it.next();
			if ((boolean) pairs.getValue()) {
				str += controller.nodeMap.get(pairs.getKey());
			}
		}

		String temp;
		// str=str.trim();
		while (str.length() > 0) {
			temp = "";
			temp += str.charAt(0);
			result += temp;
			str = str.replace(temp, "");

		}

		return result;
	}

	public String compute(String comand, String name) {
		// !compute 10 + 5 * 2

		comand = comand.replace("!compute", "");
		comand = comand.replace(" ", ""); // 10+5*2
		String parts[] = comand.split("[*+/]|-");
		comand = comand.replaceAll("[0-9]", "");

		int anz = parts.length;

		// check for credits
		int oldcred = (controller.userData.get(name));
		int cost = (anz - 1) * 50;
		if (oldcred < cost) {
			return "Insufficient Credits";
		}

		String result = "";
		int n = 0;
		for (int i = 0; i < anz - 1; i++) {
			String request = parts[i] + " " + comand.charAt(n) + " "
					+ parts[i + 1];

			/*
			 * Lab2 Task4
			 */
			if (controller.opcount.containsKey(comand.charAt(n))) {
				long temp = controller.opcount.get(comand.charAt(n));
				controller.opcount.put(comand.charAt(n), ++temp);

			} else {

				controller.opcount.put(comand.charAt(n), (long) 1);
			}
			/*
			 * */

			// find correct Node
			Iterator<Entry<String, Boolean>> it = controller.aliveMap
					.entrySet().iterator();
			String[] str = new String[controller.nodeMap.size()];
			int k = 0;
			// get online Node
			while (it.hasNext()) {
				Entry<String, Boolean> pairs = it.next();
				if ((boolean) pairs.getValue()) {

					if ((controller.nodeMap.get(pairs.getKey()).contains(""
							+ comand.charAt(n)))) {
						str[k] = pairs.getKey();
						k++;
					}

				}
			}
			// get lowest used Node with OP
			String node = "";
			int usage = Integer.MAX_VALUE;
			for (int nr = 0; nr < k; nr++) {
				int temp = controller.usageMap.get(str[nr]);
				if (temp < usage) {
					// if(controller.nodeMap)
					usage = temp;
					node = str[nr];
				}
			}

			if (node == "") {
				return "No Node for " + comand.charAt(n) + " available";
			}

			// NODE found lets gogo!.

			ObjectInputStream objectin = null;
			ComputationRequestInfo object = null;
			try {
				String host = controller.config.getString("controller.host");
				Socket socketNode = new Socket(host, Integer.parseInt(node));

				// create a reader to retrieve messages send by the server
				BufferedReader serverReader = new BufferedReader(
						new InputStreamReader(socketNode.getInputStream()));
				// create a writer to send messages to the server
				PrintWriter serverWriter = new PrintWriter(
						socketNode.getOutputStream(), true);

				// request on Node
				serverWriter.println(request);

				String tempresult = serverReader.readLine();

				/*
				 * Lab2 Task 4
				 */
				objectin = new ObjectInputStream(socketNode.getInputStream());
				object = (ComputationRequestInfo) objectin.readObject();
				if (object != null) {
					controller.logInfo.add(object);
					// System.out.println(object.getLogname()+","+object.getName()+","+object.getRequest()+","+object.getResult());
				}

			
				/*
				 * */

				socketNode.close();
				if (tempresult.startsWith("Error")) {
					return tempresult;
				}
				parts[i + 1] = tempresult;
				result = tempresult;

				// set usage
				controller.usageMap.put(node, (result.length() * 50) + usage);

			} catch (UnknownHostException e) {
				return "No Node for " + comand.charAt(n) + "available";

			} catch (IOException e) {

				// System.out.println("socket prob");
				try {
					if (objectin != null) {
						objectin.close();
					}
				} catch (IOException e1) {

					e1.printStackTrace();
				}
			} catch (ClassNotFoundException e) {

				e.printStackTrace();
			}
			// next operand
			n++;
		}
		// all OK

		controller.userData.put(name, oldcred - cost);

		return result;
	}

}
