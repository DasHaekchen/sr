package controller;

import security.CloudSecureAuthentication;
import security.SecurityException;
import util.Config;
import util.SecurityUtils;
import java.security.Security;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.SocketException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.Key;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import model.ComputationRequestInfo;
import admin.INotificationCallback;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

class AliveCheck extends Thread implements Runnable
{
	private UdpListener udpL;
	private int timeout;
	private int checkint;
	private ConcurrentHashMap<String,Boolean> map;
	private ConcurrentHashMap<String,Timestamp> maptime;

	AliveCheck(UdpListener udpL,ConcurrentHashMap<String,Boolean> map,ConcurrentHashMap<String,Timestamp> maptime ,int timeout,int checkint) {
		this.udpL=udpL;
		this.map=map;
		this.maptime=maptime;
		this.timeout=timeout;
		this.checkint=checkint;

	} 

	public void run() 
	{
		System.out.println("Checker gestartet");
		while(true){
			long currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()).getTime();

			Iterator<Entry<String, Timestamp>> it = maptime.entrySet().iterator();
			while (it.hasNext()) 
			{
				Entry<String, Timestamp> pairs = it.next();
				long dif=( currentTimestamp-pairs.getValue().getTime());
				if(dif<timeout)
				{
					map.put(pairs.getKey(), true);

				}else{
					map.put(pairs.getKey(), false);
				}
			}


			//System.out.println("Checker Tick");
			try {
				Thread.sleep(checkint);
			} catch (InterruptedException e) {

				break; 

			}

		}

	}


}



public class CloudController implements ICloudControllerCli, IAdminConsole, INotificationCallback, Runnable{

	private String componentName;
	Config config;
	private Config users;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;

	private ServerSocket serverSocket;
	private DatagramSocket datagramSocket;

	private Registry registry;
	private String bindingname;
	private String controllerhost;
	private int controllerrmiport;
	private String keysdir;

	private UdpListener UdpListen;
	private TcpListener TcpListen;
	private AliveCheck ACheck;

	private int ressources;

	public Boolean run=false;

	public ConcurrentHashMap<String,Timestamp> nodeTime =new ConcurrentHashMap<String,Timestamp>() ;
	public ConcurrentHashMap<String,Boolean> aliveMap =new ConcurrentHashMap<String, Boolean>() ;
	public ConcurrentHashMap<String,String> nodeMap =new ConcurrentHashMap<String,String>() ;
	public ConcurrentHashMap<String,Integer> usageMap =new ConcurrentHashMap<String,Integer>() ;
	public ConcurrentHashMap<String,String> ipMap =new ConcurrentHashMap<String,String>() ;

	public ArrayList<ComputationRequestInfo> logInfo = new ArrayList<ComputationRequestInfo>();
	public LinkedHashMap<Character, Long> opcount = new LinkedHashMap<Character, Long>();
	private  ExecutorService callbacks;

	public ConcurrentHashMap<String,Boolean> onlineUsers =new ConcurrentHashMap<String, Boolean>() ;
	public ConcurrentHashMap<String,Integer> userData =new ConcurrentHashMap<String,Integer>() ;
	public ConcurrentHashMap<String,String> userPass =new ConcurrentHashMap<String,String>() ;

	//private int state;
    public CloudSecureAuthentication secureAuth = null;

	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public CloudController(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;


		users=new Config("user");
        Security.addProvider(new BouncyCastleProvider());
        SecurityUtils.registerBouncyCastle();
        try {
            secureAuth = new CloudSecureAuthentication(config.getString("keys.dir"), config.getString("key"));
        } catch (SecurityException e) {
            userResponseStream.println("Error initiating security");
        }


	}

	public int getRes() {
		return ressources;
	}


	@Override
	public void run() {
		userResponseStream.println("Starte Server:...");

		//initiate userMAPS

		Set<String> keys=users.listKeys();
		int size = keys.size() /2;
		Object[] str = keys.toArray();
		String[] names = new String[size];

		for (int i=0; i<=size-1;i++)
		{
			names[i]= str[i].toString().replace(".password", "");
			onlineUsers.put(names[i], false);	
			userData.put(names[i],users.getInt(names[i]+".credits"));
			userPass.put(names[i],users.getString(names[i]+".password"));
		}

		//lab2
		ressources=config.getInt("controller.rmax");
		//

		run=true;



		try {
			datagramSocket = new DatagramSocket(config.getInt("udp.port"));
			UdpListen=new UdpListener(datagramSocket,this);
			UdpListen.start();
			//new UdpListener(datagramSocket).start();
			ACheck=new AliveCheck(UdpListen,aliveMap,nodeTime, config.getInt("node.timeout"),config.getInt("node.checkPeriod"));
			ACheck.start();
			userResponseStream.println("UDP port Listener open.");
		} catch (SocketException e) {
			userResponseStream.println(e.getClass().getSimpleName() + ": "
					+ e.getMessage());
			run=false;
		}


		try {
			serverSocket = new ServerSocket(config.getInt("tcp.port"));
			TcpListen= new TcpListener(serverSocket,this);
			TcpListen.start();
			TcpListen.getNodes();

			userResponseStream.println("TCP worker ready.");
		} catch (IOException e1) {
			userResponseStream.println(e1.getClass().getSimpleName() + ": "
					+ e1.getMessage());
		}

		/**Lab 2 Task 4 start
		 * Daten aus Config File auslesen
		 * RMI Registry erzeugen und exportieren
		 * 
		 * Threadpool wird erzeugt um !subscribe-Methode Threads ausfuehren zu koennen
		 * */
		bindingname = config.getString("binding.name");
		controllerhost = config.getString("controller.host");
		controllerrmiport = config.getInt("controller.rmi.port");
		keysdir = config.getString("keys.dir");

		try {
			registry = LocateRegistry.createRegistry(controllerrmiport);

			IAdminConsole remote = (IAdminConsole) UnicastRemoteObject.exportObject(this, 0);
			registry.bind(bindingname, remote);

		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			userResponseStream.println(e1.getClass().getSimpleName() + ": "
					+ e1.getMessage());
		}
		catch (AlreadyBoundException e2) {
			// TODO Auto-generated catch block
			userResponseStream.println(e2.getClass().getSimpleName() + ": "
					+ e2.getMessage());
		}

		callbacks = Executors.newCachedThreadPool();
		/** Lab 2 Task 4 end
		 * */

		BufferedReader reader = new BufferedReader(new InputStreamReader(userRequestStream));
		String input="";

		while(run){
			try {
				input=reader.readLine();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				switch ( input )
				{
				case "!exit":
					run=false;
					exit();
					break;
				case "!users":

					users();
					break;
				case "!nodes":
					nodes();
					break;
				case ""
				+ "":
					break;
				default:
					userResponseStream.println("Unknown Command " + input);

				}
			} catch (IOException e) {

				userResponseStream.println(e.getClass().getSimpleName() + ": "
						+ e.getMessage());
			}

		}

		try {
			reader.close();
		} catch (IOException e) {

			userResponseStream.println(e.getClass().getSimpleName() + ": "
					+ e.getMessage());
		}
	}

	@Override
	public String nodes() throws IOException {

		Iterator<Entry<String, String>> it = nodeMap.entrySet().iterator();
		int i=1;
		while (it.hasNext()) {
			Entry<String, String> pairs = it.next();
			String node=pairs.getKey();
			String off="offline";
			if(aliveMap.get(node)==true)off="online";
			String ip= ipMap.get(node);

			userResponseStream.println(ip+" Port: "+node+" "+off+" Usage: "+usageMap.get(node));
			i++;	
		}


		return null;
	}

	@Override
	public String users() throws IOException {

		//aliveMap.put("0300",true);

		Set<String> keys=users.listKeys();
		int size = keys.size() /2;
		Object[] str = keys.toArray();
		String[] names = new String[size];
		String output ="";

		for (int i=0; i<=size-1;i++)
		{
			names[i]= str[i].toString().replace(".credits", "");
			String off="offline";
			if(onlineUsers.get(names[i])==true)off="online";
			//output=names[i] + "\n";
			userResponseStream.println(names[i]+" "+off+" Credits: "+userData.get(names[i]));
		}

		return null;
	}

	@Override
	public String exit() throws IOException {

		userResponseStream.println("Shutdown System.");
		ACheck.stop();

		if (datagramSocket != null)
			datagramSocket.close();


		if (serverSocket != null)
			try {
				serverSocket.close();
			} catch (IOException e) {
				// Ignored because we cannot handle it
			}


		/**Lab2 Task 4
		 * */
		try {
			UnicastRemoteObject.unexportObject(this, true);
			registry.unbind(bindingname);
			callbacks.shutdownNow();
		} catch (NotBoundException e) {
			userResponseStream.println(e.getClass().getSimpleName() + ": "
					+ e.getMessage());
		}

		/**
		 * */
		return null;
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link CloudController}
	 *            component
	 */
	public static void main(String[] args) {
		CloudController cloudController = new CloudController(args[0],
				new Config("controller"), System.in, System.out);
		
		cloudController.run();

	}
	/**
	 * neuer Thread wird erzeugt und zu Pool hinzugefuegt
	 * */
	@Override
	public boolean subscribe(String username, int credits,
			INotificationCallback callback) throws RemoteException {

		CallbackHandler temp = new CallbackHandler(username, credits, callback, this);
		callbacks.execute(temp);
		return true;
	}

	@Override
	public List<ComputationRequestInfo> getLogs() throws RemoteException {
	
		if(logInfo.size()>2){
		
			synchronized (logInfo){
				Collections.sort(logInfo);
			}
		}
		return logInfo;
	}

	@Override
	public LinkedHashMap<Character, Long> statistics() throws RemoteException {

		return opcount;
	}

	@Override
	public void notify(String username, int credits) throws RemoteException {


	}

	@Override
	public Key getControllerPublicKey() throws RemoteException {
		// nicht notwendig fuer Lab2
		return null;
	}

	@Override
	public void setUserPublicKey(String username, byte[] key)
			throws RemoteException {
		// nicht notwendig fuer Lab2

	}

}

