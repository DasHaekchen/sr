package controller;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

public class UdpListener extends Thread {

	private DatagramSocket datagramSocket;
	private CloudController controller;
	//public ConcurrentHashMap<String,Boolean> aliveMap =new ConcurrentHashMap<String, Boolean>() ;

	public UdpListener(DatagramSocket datagramSocket,CloudController controller) {
		this.datagramSocket = datagramSocket;
		this.controller=controller;
	}

	
	
public void run() {
	
	byte[] buffer;
	DatagramPacket packet;
	
			while (controller.run) 
			{
				try {
				buffer = new byte[128];
				
				packet = new DatagramPacket(buffer, buffer.length);
	
				// wait for incoming packets from client
				datagramSocket.receive(packet);
			
				// get the data from the packet
				String request = new String(packet.getData());
				
				String[] parts = request.split(" ");
				
				

					if (request.startsWith("!alive"))
					{
						String tcpport= parts[1];	
						//aliveMap.put(tcpport, true);
						Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
						controller.nodeTime.put(tcpport, currentTimestamp);
						
						controller.nodeMap.putIfAbsent(tcpport,parts[2].trim());
						controller.usageMap.putIfAbsent(tcpport,0);
						
						String ip=packet.getSocketAddress().toString();
						String[] partsIp = ip.split(":");
						partsIp[0]=partsIp[0].substring(1);
						controller.ipMap.putIfAbsent(tcpport,partsIp[0]);
						
						
					}else{
						if(request.startsWith("!hello"))
						{
							SocketAddress ip=packet.getSocketAddress();
							//sending NodeList + ressources 
							Iterator<Entry<String, String>> it = controller.ipMap.entrySet().iterator();;
							String response="!init#";
							synchronized (it){  
								
							while (it.hasNext()) 
							{
								Entry<String, String> pairs = it.next();
								if(controller.aliveMap.get(pairs.getKey())){
								response+= pairs.getValue()+":"+pairs.getKey()+"#";
								}
							}
							
								response+= controller.getRes();
							}
								
								DatagramSocket socket = new DatagramSocket();
								buffer = new byte[1024];
								buffer = response.getBytes();
								
								packet = new DatagramPacket(buffer, buffer.length,
										ip);
								socket.send(packet);
								
			
						}
					}
					
						
				
			
		
			
		} catch (IOException e) {
				
			datagramSocket.close();
			break;
		}
	}
}	
	

/* unused due chagnes
	public Set<String> getAlive() 
	{
		Set<String> keys =aliveMap.keySet();
		return keys;
		
	}	

	public void clearAlive() 
	{
		aliveMap.clear();	
	}	
	*/

}



