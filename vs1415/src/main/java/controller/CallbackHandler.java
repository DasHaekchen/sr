package controller;

import java.rmi.RemoteException;

import admin.INotificationCallback;

public class CallbackHandler extends Thread {

	private  CloudController controller;
	private String user;
	private int credits;
	private INotificationCallback callback;
	
	public CallbackHandler(String username, int credits,
			INotificationCallback callback,CloudController controller){
		this.controller=controller;
		user = username;
		this.credits=credits;
		this.callback = callback;
	}
	
	public void run(){

		while(!this.isInterrupted()){
			
			if(controller.userData.containsKey(user) && (controller.userData.get(user) < credits)){
				try {
					callback.notify(user,credits);
					this.interrupt();
					break;
				} catch (RemoteException e) {
					
					e.printStackTrace();
				}
			}
		}
	}
}
