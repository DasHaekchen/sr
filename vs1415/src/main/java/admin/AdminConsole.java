package admin;

import controller.IAdminConsole;
import model.ComputationRequestInfo;
import util.Config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.security.Key;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Please note that this class is not needed for Lab 1, but will later be
 * used in Lab 2. Hence, you do not have to implement it for the first
 * submission.
 */
public class AdminConsole implements IAdminConsole, Runnable,INotificationCallback {

	private String componentName;
	private Config config;
	private InputStream userRequestStream;
	private PrintStream userResponseStream;

	private Registry registry;
	private IAdminConsole console;
	private String bindingname;
	private String controllerhost;
	private int controllerrmiport;
	private String keysdir;
	
	private INotificationCallback remote;

	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public AdminConsole(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		this.componentName = componentName;
		this.config = config;
		this.userRequestStream = userRequestStream;
		this.userResponseStream = userResponseStream;
	}

	@Override
	public void run() {

		bindingname = config.getString("binding.name");
		controllerhost = config.getString("controller.host");
		controllerrmiport = config.getInt("controller.rmi.port");
		keysdir = config.getString("keys.dir");
		
		BufferedReader userInputReader = null;

		try {
			registry = LocateRegistry.getRegistry(controllerhost, controllerrmiport);
			console = (IAdminConsole) registry.lookup(bindingname);
			
			remote = (INotificationCallback) UnicastRemoteObject.exportObject(this, 0);
			
		} catch (RemoteException e) {
			userResponseStream.println(e.getClass().getSimpleName() + ": "
					+ e.getMessage());
		} catch (NotBoundException e) {
			userResponseStream.println(e.getClass().getSimpleName() + ": "
					+ e.getMessage());
		}finally{
			if(userInputReader != null){
				try {
					userInputReader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		//userResponseStream.print("Admin RMI gebunden: "+bindingname+" "+controllerrmiport);
		
		userInputReader = new BufferedReader(new InputStreamReader(
				userRequestStream));
		userResponseStream.println("Admin Console started! Enter command!");

		try {

			while(true){
				String input = userInputReader.readLine();
				if(input == null){
					break;
				}
				else if(input.startsWith("!getLogs")){
					List<ComputationRequestInfo> response = getLogs();
					String ausgabe="";
					for(int i=0; i<response.size(); i++){
						ausgabe += response.get(i).getLogname()+" ["+response.get(i).getName()+"]: "+response.get(i).getRequest()+" = "+response.get(i).getResult()+"\n";
					}
					userResponseStream.println(ausgabe);
				}
				else if(input.startsWith("!statistics")){
					LinkedHashMap<Character, Long> response = statistics();
					
					String ausgabe="";
					Set set = response.entrySet();
					Iterator iter = set.iterator();
					
					while(iter.hasNext()){
						Map.Entry e = (Map.Entry) iter.next();
						ausgabe += e.getKey()+" "+ e.getValue()+"\n";
					}
					userResponseStream.println(ausgabe);	
				}
				else if(input.startsWith("!subscribe")){
					String parts[]= input.split(" ");

					if(parts.length<3 || parts.length>3){
						userResponseStream.println("Command !subscribe needs username and credits!");
					}
					else{
						try{
							subscribe(parts[1],Integer.parseInt(parts[2]), remote);
						}catch(RemoteException e){
							userResponseStream.println("RemotException");
						}
						userResponseStream.println("If "+parts[1]+" has less than "+parts[2]+" credits, you will be notified.");
					}
				}
				else{
					userResponseStream.println("Admin, this was a wrong Command!");
				}
			}
		} catch (IOException e) {
		
			e.printStackTrace();
		}



	}

	@Override
	public boolean subscribe(String username, int credits,
			INotificationCallback callback) throws RemoteException {
		console.subscribe(username, credits, callback);
		return true;
	}

	@Override
	public List<ComputationRequestInfo> getLogs() throws RemoteException {
		return console.getLogs();
	}

	@Override
	public LinkedHashMap<Character, Long> statistics() throws RemoteException {
		return console.statistics();
	}

	@Override
	public Key getControllerPublicKey() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setUserPublicKey(String username, byte[] key)
			throws RemoteException {
		// TODO Auto-generated method stub
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link AdminConsole}
	 *            component
	 */
	public static void main(String[] args) {
		AdminConsole adminConsole = new AdminConsole(args[0], new Config(
				"admin"), System.in, System.out);
		/*Lab2 Task 4
		 * */
		
		adminConsole.run();

	}
	
	@Override
	public void notify(String username, int credits) throws RemoteException {

		userResponseStream.println("Notification "+username+" has less than "+credits+" credits.");
	}
}