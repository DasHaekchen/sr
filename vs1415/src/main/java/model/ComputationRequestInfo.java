package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

import node.Node;

/**
 * Please note that this class is not needed for Lab 1, but will later be
 * used in Lab 2. Hence, you do not have to implement it for the first
 * submission.
 */
public class ComputationRequestInfo implements Serializable, Comparable<ComputationRequestInfo> {

	private String logname;
	private String name;
	private String request;
	private String result;

	public ComputationRequestInfo(String logname, Node node, String text, String result){
		this.logname = logname;
		name = node.getName();
		request = text;
		this.result = result;

	}
	
	public String getLogname(){
		return logname;
	}
	
	public String getName(){
		return name;
	}
	public String getRequest(){
		return request;
	}
	public String getResult(){
		return result;
	}

	@Override
	public int compareTo(ComputationRequestInfo o) {
		
		return logname.compareTo(o.getLogname());
	}

}

