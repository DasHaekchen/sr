package at.ac.tuwien.palloncino;


import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * @author Maria Zisser <e1026596@student.tuwien.ac.at>
 * 
 * Objekt das sich von oben nach unten bewegt,wird im LevelSurfaceView gezeichnet
 * um zu wissen wann Display Ende erreicht wird, wird boolean unten verwendet
 */
public class LevelObject {

	private LevelSurfaceView levels;
	private Bitmap bild;
	private int breite;
	private int hoehe;
	private int x;
	private int y;
	private int yrichtung;
	private int xrichtung;
	private boolean unten;

	/**
	 * @param levels LevelObject wird bestimmtem SurfaceView zugeordnet, auf dem es dann gezeichnet werden kann
	 * @param bild...Bitmap das dargestellt werden soll
	 * x...Koordianten an der LevelObject zu Beginn gezeichnet wird, wird zufaellig berechnet
	 * y...Koordinate ist zu Beginn immer 0
	 * */
	public LevelObject(LevelSurfaceView levels, Bitmap bild){
		this.levels = levels;
		this.bild = bild;
		this.breite = bild.getWidth();
		this.hoehe = bild.getHeight();
		this.x = (int) (Math.random()*levels.getWidth());
		this.y=0;
		unten = false;
		yrichtung =5;
		xrichtung = 3;
	}

	/**
	 * veraendert x und y Koordinate des Bildes mithilfe der festgelegten Werte, 
	 * die hinzugerechnet werden.
	 * wenn x Display Breite erreicht hat, wird die Richtung geaendert
	 * wenn y Display Hoehe erreicht hat, wird unten auf true gesetzt.
	 * */
	private void bewegen(){
		if(x > levels.getWidth() - breite - xrichtung||x+xrichtung<0){
			xrichtung = -xrichtung;
		}
		x+=xrichtung;
		if(y < levels.getHeight()-100){            // veraendert y-Wert um Objekte zu bewegen
			y = y+yrichtung;
		}
		if(y>=levels.getHeight()-100){
			unten = true;
		}
	}
	
	/**
	 * eigene onDraw Methode, um Koordinaten zuerst zu aktualisieren, zu bewegen,
	 * und dann Bild des LevelObejcts auf den Canvas der uebergeben wird zu zeichenen
	 * */
	public void onDraw(Canvas c){
		bewegen();
		c.drawBitmap(bild,x,y,null);
	}
	
	/**
	 * Getter und Setter der Variablen
	 */
	public int getX(){
		return x;
	}
	
	public int getY() {
		return y;
	}
	public void setUnten(boolean b){
		unten = b;
	}
	public boolean getUnten(){
		return unten;
	}
	
	public Bitmap getBild(){
		return bild;
	}
	public int getXunten(){
		return x+breite;
	}
	public int getYunten(){
		return y+hoehe;
	}
	
	

}
