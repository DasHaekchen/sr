 package at.ac.tuwien.palloncino;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.content.Intent;
import android.database.Cursor;

import android.support.v4.app.FragmentActivity;
// imports of the support library
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;

/**
 * @author Saskia Reimerth <e0807136@student.tuwien.ac.at>
 * @see Tuwien Multimedia Reference HighscoreSQLite
 */
public class ShowHighscore extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {
	private static final String TAG = ShowHighscore.class.getSimpleName();
	
	private SimpleCursorAdapter dbAdapter;
		
	private ListView listView;
	private LinearLayout progressBarLayout;
	
	/**
	 * Creates the ui elements for the highscore display
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_showhighscore);
		
		// get the needed views
		listView = (ListView) findViewById(R.id.listViewHighscore);
		// if the list is empty, a textview is shown instead
		listView.setEmptyView(findViewById(R.id.emptyHighscoreList));
		// the progress spinner that is shown during the loading time
		progressBarLayout = (LinearLayout) findViewById(R.id.pbHighscore);

	    // fetch data from the database and shows it in the list
		showData();
		
	    // registers the context menu
	    registerForContextMenu(listView);
	}
	
	/**
	 * Method to show the data from the database
	 */
	public void showData() {
	    // shows loading status
	    progressBarLayout.setVisibility(View.VISIBLE);
	    listView.setVisibility(View.GONE);
		
		// initializes LoaderManager to load the data in a background task
	    getSupportLoaderManager().initLoader(0, null, this);
	    		
		// table columns of the data to be displayed
	    String[] from = new String[] { HighscoreTable.DB_COLUMN_NAME, HighscoreTable.DB_COLUMN_PUNKTE };
	    // labes of the textviews to be shown in
	    int[] to = new int[] { R.id.labelName, R.id.labelScore };
	    
	    dbAdapter = new SimpleCursorAdapter(this, R.layout.hs_entry, null, from, to, 0);
	    //lists the entries
	    listView.setAdapter(dbAdapter);
	}
	
	/**
	 * Sets up the cursor object to load the data from the database
	 * @see android.support.v4.app.LoaderManager.LoaderCallbacks#onCreateLoader(int, android.os.Bundle)
	 */
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {		
	    // set the columns to load
		String[] projection = { HighscoreTable.DB_COLUMN_ID, HighscoreTable.DB_COLUMN_NAME, HighscoreTable.DB_COLUMN_PUNKTE };
	    // loads entries ordered by punkte/score in descending order
		CursorLoader cursorLoader = new CursorLoader(this,
	        HSContentProvider.CONTENT_URI, projection, null, null, HighscoreTable.DB_COLUMN_PUNKTE + " DESC");
	    
	    return cursorLoader;
	}

	/**
	 * When the loading is finished, the progress bar gets masked out and the list is shown
	 * @see android.support.v4.app.LoaderManager.LoaderCallbacks#onLoadFinished(android.support.v4.content.Loader, java.lang.Object)
	 */
	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		Log.d(TAG, "Finished to load data");
		Log.d(TAG, "Datasets loaded: " + data.getCount());		
		dbAdapter.swapCursor(data);
		// changes the visibilities to hide the progress bar and show the list
	    progressBarLayout.setVisibility(View.GONE);
	    listView.setVisibility(View.VISIBLE);
	}

	/**
	 * At reset the Cursor is set to null (empty)
	 * @see android.support.v4.app.LoaderManager.LoaderCallbacks#onLoaderReset(android.support.v4.content.Loader)
	 */
	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	    dbAdapter.swapCursor(null);
	}

	/**
	 * When the back button gets pressed, the Welcome_screen is started
	 * @see android.support.v4.app.FragmentActivity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		startActivity(new Intent(getApplicationContext(), Welcome_Screen.class));
	}
	
	
}
