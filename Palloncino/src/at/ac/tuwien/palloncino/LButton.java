package at.ac.tuwien.palloncino;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;

/**
 * @author Maria Zisser <e1026596@student.tuwien.ac.at>
 * 
 * erstellt Button der in LevelSurfaceView aufgerufen und
 * fuer Steuerung des Ballons verwendet wird
 */
public class LButton {

	public RectF rect;
	public Matrix matrix = new Matrix();
	Bitmap picture;
	float width;
	float height;   
	
	/**
	 * @param width, height, picture Bild, Breite und Hoehe des uebergebenen Bildes
	 * rect Rechteck mit den Abmessungen des Bildes, um es dann an beliebiger Position zeichnen zu koennen
	 * */ 
	public LButton(float width, float height, Bitmap picture)
	{
		this.width = width;
		this.height = height;
		this.picture = picture;

		rect = new RectF(0, 0, width, height);
	}

	/**
	 * @param x, y Koordinaten an die der Button gezeichnet werden soll
	 */
	public void setPosition(float x, float y)
	{
		matrix.setTranslate(x, y);
		matrix.mapRect(rect);
	}

	public void draw(Canvas canvas)
	{
		canvas.drawBitmap(picture, matrix, null);
	}

}
