package at.ac.tuwien.palloncino;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * @author Saskia Reimerth <e0807136@student.tuwien.ac.at>
 * @see Tuwien Multimedia Reference HighscoreSQLite
 */
public class HighscoreTable {

	private static final String TAG = OpenHandler.class.getSimpleName();
	
	// name and column names of the table
	public static final String DB_TABLE = "highscore";
	public static final String DB_COLUMN_ID = "_id";
	public static final String DB_COLUMN_NAME = "name";
	public static final String DB_COLUMN_PUNKTE = "punkte";
	
	// the sql command for creating the table
	private static final String DB_TABLE_CREATE = 
			"CREATE TABLE " + DB_TABLE + " (" + DB_COLUMN_ID +
			" INTEGER PRIMARY KEY AUTOINCREMENT, " + DB_COLUMN_NAME +
			" TEXT not null, " + DB_COLUMN_PUNKTE + " INTEGER);";
	
	// the sql command for deleting the table
	private static final String DB_TABLE_DELETE =
			"DROP TABLE IF EXISTS " + DB_TABLE;
	
	/**
	 * When the class is created, the sql statement to create
	 * the table is executed.
	 * @param db the given SQLite database
	 */
	public static void onCreate(SQLiteDatabase db){
		db.execSQL(DB_TABLE_CREATE);
	}
	
	/**
	 * This method upgrades the table to a new version by first deleting the
	 * existing table and then creating a new one.
	 * @param db the given SQLite database
	 * @param oldVersion the version number of the supposedly old table version
	 * @param newVersion the version number of the new one
	 */
	public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
		db.execSQL(DB_TABLE_DELETE);
		onCreate(db);
	}
	
	
}
