package at.ac.tuwien.palloncino;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
/**
 * @author Maria Zisser
 * 
 * Win Ansicht, wenn Levels abgeschlossen
 * enthaelt Animationen der Bilder
 * */
public class Win extends Activity {

	/**
	 * laedt Win View und Animationen
	 * */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_win);
		
		Animation anim = AnimationUtils.loadAnimation(this, R.anim.winanim);
		ImageView imageView = (ImageView) findViewById(R.id.imageView1);
		anim.reset();
		imageView.clearAnimation();
		imageView.startAnimation(anim);
		
		Animation anim2 = AnimationUtils.loadAnimation(this, R.anim.textanim);
		ImageView textView = (ImageView) findViewById(R.id.wintext);
		anim2.reset();
		textView.clearAnimation();
		textView.startAnimation(anim2);
	}

	/**
	 * @param v View der aktuellen Ansicht wird uebergeben
	 * Activity Levels wird aufgerufen, diese beendet
	 * */
	public void replay(View v){
		Intent intent = new Intent(getApplicationContext(), Levels.class);
		startActivity(intent);
		this.finish();
	}
	
	/**
	 * @param v...View der aktuellen Ansicht wird uebergeben
	 *AddHighscore Activity wird aufgerufen um Punkte speichern zu k�nnen
	 *deswegen wird als Extra score hinzugefuegt und intent dann gestartet
	 * */
	public void savePoints(View v){
		Intent intent = new Intent(getApplicationContext(), AddHighscore.class);
		intent.putExtra("score", this.getIntent().getExtras().getInt("score"));
		startActivity(intent);
		this.finish();
	}
	
	protected void onDestroy(){
		super.onDestroy();
	}

}
