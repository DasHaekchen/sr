package at.ac.tuwien.palloncino;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

/**
 * @author Maria Zisser <e1026596@student.tuwien.ac.at>, Saskia Reimerth <e0807136@student.tuwien.ac.at>
 */
public class GameOver extends Activity{
  /**
   * @author Maria Zisser <e1026596@student.tuwien.ac.at>
   * Activity wird erzeugt, 
   * Animationen der Bilder werden geladen und gestartet
   * */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_game_over);
	
		Animation anim = AnimationUtils.loadAnimation(this, R.anim.scale);
		ImageView imageView = (ImageView) findViewById(R.id.imageView1);
		anim.reset();
		imageView.clearAnimation();
		imageView.startAnimation(anim);
	}
	
	/**
	 * Starts the GameOver animation
	 */
	public void startViewAnimation(View view){
		Animation anim = AnimationUtils.loadAnimation(this, R.anim.scale);
		ImageView imageView = (ImageView) findViewById(R.id.imageView1);
		anim.reset();
		imageView.clearAnimation();
		imageView.startAnimation(anim);
	}
	
	/**
	 * When the "back" button gets pressed, the GameOver view stops and
	 * the addHighscore activity should get started
	 * @author Saskia Reimerth <e0807136@student.tuwien.ac.at>
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		// creates a new intent and adds the score given by Levels.class
		Intent intent = new Intent(getApplicationContext(), AddHighscore.class);
		intent.putExtra("score", this.getIntent().getExtras().getInt("score"));
		startActivity(intent);
		this.finish();
	}
	/**
	 * @author Maria Zisser
	 * wird Replay Button gedrueckt, wird neue Levels Activity aufgerufen
	 * und GameOver Activity beendet
	 * */
	public void replay(View v){
		Intent intent = new Intent(getApplicationContext(), Levels.class);
		startActivity(intent);
		this.finish();
	}
	
	/**
	 * @author Maria Zisser
	 * wird SavePoints Button gedrueckt, wird neue AddHighscore Activity aufgerufen
	 * und GameOver Activity beendet. So koennen erreichte Punkte gespeichert werden
	 * */
	public void savePoints(View v){
		Intent intent = new Intent(getApplicationContext(), AddHighscore.class);
		intent.putExtra("score", this.getIntent().getExtras().getInt("score"));
		startActivity(intent);
		this.finish();
	}
	
	protected void onDestroy(){
	
		super.onDestroy();
	}
}
