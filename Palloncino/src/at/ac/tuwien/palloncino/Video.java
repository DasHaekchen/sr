package at.ac.tuwien.palloncino;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.VideoView;

/**
 * @author Maria Zisser <1026596>
 * 
 * spielt Intro Video ab
 * Quellen: Vorlesungsfolien/MediaPlayback
 */
public class Video extends Activity implements OnClickListener, OnCompletionListener{

	VideoView myVideoView;
	Button skipButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		
		setContentView(R.layout.activity_video);
		
		myVideoView = (VideoView)findViewById(R.id.myvideoView);
		
		Uri video = Uri.parse("android.resource://"+ getPackageName() + "/" + R.raw.introapp);
		myVideoView.setVideoURI(video);
		
		skipButton = (Button) findViewById(R.id.mybutton_skip);
		skipButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				myVideoView.stopPlayback();
				Intent intent = new Intent(v.getContext(), Welcome_Screen.class);			
				startActivityForResult(intent, 0);
			}
		});
		myVideoView.setOnCompletionListener(new OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer mp) {
				Intent intent = new Intent(getApplicationContext(), Welcome_Screen.class);
				startActivityForResult(intent, 0);
			}
		});
		myVideoView.start();
		
		
	}

	/**
	 * creates the options menu (if the phone has a menu button
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		getMenuInflater().inflate(R.menu.video, menu);
		return true;
	}
	
	public void onDestroy(){
		
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onCompletion(MediaPlayer arg0) {
		
	}
	

}
