package at.ac.tuwien.palloncino;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

/**
 * @author Maria Zisser
 * 
 * Animation eines Objektes waehrend des Spiels
 * fast gleicher Aufbau wie LevelObject. es wird ebenso die Position des Objekts angepasst,
 * so dass es von oben nach unten sich bewegt
 * 
 * zusaetzlich durchlauft das Bild mehrere Darstellungen, so dass eine
 * Animation entsteht
 */
public class Sprite {

	private static final String TAG = Welcome_Screen.class.getSimpleName();
	private static final int HELP_DIALOG = 1;
			
	private int currentFrame;			
	private int spalte = 2;
	private int reihe = 4;
	private int x;
	private int y;
	private int xrichtung;
	private int yrichtung;
	private int breite;
	private int hoehe;
	private Bitmap bild;
	private LevelSurfaceView levels;
	private boolean unten;
	private int pointer;
	
	/**
	 * @param levels welchem SurfaceView das Objekt zugewiesen wird
	 * @param bild gesamte Abfolge der Bilder. von diesem werden die einzelnen Teilbilder entnommen,
	 * die hintereinander angezeigt werden sollen.
	 * */
	public Sprite(LevelSurfaceView levels, Bitmap bild){
		this.levels = levels;
		this.bild = bild;
		this.breite = bild.getWidth()/spalte;
		this.hoehe = bild.getHeight()/reihe;
		this.x = (int) (Math.random()*levels.getWidth());
		this.y=0;
		unten = false;
		
		yrichtung =6;
		xrichtung = 2;
		pointer=0;
	}

	/**
	 * veraendert x und y Koordinate des Bildes mithilfe der konstanten Werte, 
	 * die hinzugerechnet werden.
	 * wenn x Display Breite erreicht hat, wird die Richtung geaendert
	 * wenn y Display Hoehe erreicht hat, wird unten auf true gesetzt.
	 * */
	private void bewegen(){
			if (x > levels.getWidth() - breite - xrichtung || x + xrichtung < 0) {
				xrichtung = -xrichtung;
			}
			x = (x + xrichtung);
			if (y < levels.getHeight() - 100) { // veraendert y-Wert um Objekte zu bewegen
				y = (y + yrichtung);

			}
			if (y >= levels.getHeight() - 100) {
				unten = true;
			}
	}

	/**
	 * aktueller Frame, der angezeigt werden soll, wird berechnet und
	 * dann auf Canvas gezeichnet
	 * */
	public void onDraw(Canvas c){
		bewegen();
		currentFrame = ++currentFrame % spalte;
		int sX=currentFrame*breite;
		int sY=getSprite()*hoehe;
		Rect source = new Rect(sX,sY, sX+breite, sY+hoehe);
		Rect destRect = new Rect(x, y, x + breite, y + hoehe);
		if(unten==false){
		c.drawBitmap(bild, source,destRect,null);
		}
	}
	
	private int[]bilder ={0,0,1,1,2,2,3,3};
	
	private int getSprite(){
		pointer++;
		return bilder[pointer%8];
	}
	
	public int getX(){
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public Bitmap getBild(){
		return bild;
	}
	public int getXunten(){
		return x+breite;
	}
	public int getYunten(){
		return y+hoehe;
	}
	
	public void setUnten(boolean b){
		unten = b;
	}
	public boolean getUnten(){
		return unten;
	}
	
	
}
