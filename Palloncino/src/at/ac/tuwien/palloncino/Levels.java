package at.ac.tuwien.palloncino;

import java.io.IOException;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;


/**
 * @author Maria Zisser <e1026596@student.tuwien.ac.at>, Saskia Reimerth <e0807136@student.tuwien.ac.at>
 * 
 * Activity von der aus Level koordiniert wird. hier kann LevelSurfaceView fuer Leveldesign und fuer LevelThread, 
 * Media Player fuer Hintergrundmusik, Buttons (Musik mute,Help), Hilfetext aufgerufen werden
 * Musikeffekte fuer Level
 * @see http://www.soundjay.com/wind-sound-effect.html,http://www.dccl.org/Sounds/sound.htm
 */
public class Levels extends Activity implements OnClickListener, OnLoadCompleteListener{
	LevelSurfaceView view;
	private MediaPlayer backround, score, minuspunkt;
	private int punkte;
	private boolean start = true;
	private boolean pause = false;
	private long pauseTime, starttime;
	private boolean onPauseSecondTime = false;
	private boolean pauseStumm;
	public SoundPool sp;
	public int soundId_coin, soundId_coin2, soundId_bird, soundId_cloud, soundId_ufo;
	private int coin=0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		view=new LevelSurfaceView(this);
		setContentView(view);

		score = MediaPlayer.create(this, R.raw.airplanedingsoundbible);
		//minuspunkt = MediaPlayer.create(this, R.raw.minuspoint);
		backround = MediaPlayer.create(this, R.raw.joshwoodwardmorningblueauszug);
		backround.start();
		backround.setLooping(true);

		sp = new SoundPool(3, AudioManager.STREAM_MUSIC,0);
		sp.setOnLoadCompleteListener(this);
		soundId_coin = sp.load(this, R.raw.coin_5,1);
		soundId_coin2 = sp.load(this, R.raw.coindrop4,1);

		soundId_bird = sp.load(this, R.raw.bird,1);
		soundId_cloud = sp.load(this, R.raw.thunder,1);

		soundId_ufo =sp.load(this, R.raw.airbrake,1);



	}

	protected void onDestroy(){
		//backround.stop();
		backround.release();
		super.onDestroy();
	}
	protected void onResume(){
		super.onResume();
		if(!start){
			pause = true;
		}
	}
	protected void onPause(){
		super.onPause();
		view.pauseThread();
		starttime = System.currentTimeMillis();
		onPauseSecondTime = true;
		start=false;

	}

	@Override
	public void onStop(){

		super.onStop();
		backround.release();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}

	/**
	 * wird die Zurueck Taste gedrueckt wird zum WelcomeScreen gewaechselt und
	 * Levels.java beendet
	 * */
	@Override
	public void onBackPressed(){
		Intent intent = new Intent(getApplicationContext(), Welcome_Screen.class);
		startActivity(intent);
		this.finish();
	}

	/**
	 * ueberprueft ob Level schon pausiert ist. wenn ja wird Musik wieder aktiviert,
	 * Levelzeit aktualisiert, und LevelSurfaceView wieder aktiviert
	 * ansonsten wird LevelSurfaceView pausiert und Musik ausgeschalten 
	 * */
	public void pauseLevel(){
		if(pause){
			pause=false;
			if(pauseStumm == false){
				musicOnOff();}
			pauseTime=System.currentTimeMillis()-starttime+pauseTime;
			onPauseSecondTime=false;
			view.resumeThread();
		}
		else{
			Toast.makeText(this, R.string.msgPausiert, Toast.LENGTH_LONG).show();
			view.pauseThread();
			pause=true;
			pauseStumm = view.getStumm();
			if(pauseStumm == false){
				musicOnOff();} 
			starttime = System.currentTimeMillis();
			onPauseSecondTime = true;
		}
	}

	public boolean getPause(){
		return pause;
	}

	/**
	 * ruft neue Activity GameOver auf und uebergibt score,
	 * um sie fuer Speicherung im Highscore etc. weiterverwenden zu koennen
	 * beendet aktuelle Levels Activitiy
	 * 
	 * */
	public void onGameOver(){
		musicOnOff();
		Intent intent = new Intent(getApplicationContext(), GameOver.class);
		intent.putExtra("score", view.getScore());
		startActivity(intent);
		this.finish();
	}

	/**
	 * ruft neue Activity Win auf und uebergibt score,
	 * um sie fuer Speicherung im Highscore etc. weiterverwenden zu koennen
	 * beendet aktuelle Levels Activitiy
	 * */
	public void win(){
		Intent intent = new Intent(getApplicationContext(), Win.class);
		intent.putExtra("score", view.getScore());
		startActivity(intent);
		this.finish();
	}

	/**
	 * Methode fuer An/Ausschalten der Musik waehrend des Spiels
	 * wenn Musik schon aus, und diese Methode wird aufgerufen, wird sie wieder aktiviert.
	 * ist Musik an, wird Musik auf stumm gesetzt und Hintergrundmusik pausiert
	 * */
	public void musicOnOff(){
		if (view.getStumm()) {
			try {
				view.setStumm(false);
				backround.start();
				backround.setLooping(true);

			} catch (IllegalStateException e) {
				e.printStackTrace();
			} 
		}
		else{
			if (!view.getStumm()) {
				view.setStumm(true);
				backround.pause();
			}
		}
	}

	public void onDismiss(DialogInterface dialog){
		pauseLevel();
	}
	public long getPauseTime() {
		return pauseTime;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.palloncino__main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.resume:
			if(pause){
				pauseLevel();
			}
			return true;
		case R.id.help:
			showDialog(1);
			return true;
		default:
			return false;
		}
	}

	public void showHelp(){
		showDialog(1);
	}

	@SuppressWarnings("deprecation")
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 1:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.helphead);
			builder.setMessage(R.string.help_text);
			builder.setCancelable(true);
			return builder.create();
		default:
			return super.onCreateDialog(id);
		}

	}

	public void getCoinMusik(){
		AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		float actualVolume = (float) audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC);
		float maxVolume = (float) audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		float volume = actualVolume / maxVolume;

		if(coin%2==0){
			sp.play(soundId_coin, volume, volume, 1, 0, 1f);
			coin++;
		}
		else{
			sp.play(soundId_coin2, volume, volume, 1, 0, 1f);
			coin++;
		}
	}

	public void getBirdMusik(){
		AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		float actualVolume = (float) audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC);
		float maxVolume = (float) audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		float volume = actualVolume / maxVolume;
		
		sp.play(soundId_bird, volume, volume, 1, 0, 1f);
	}
	public void getCloudMusic(){
		AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		float actualVolume = (float) audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC);
		float maxVolume = (float) audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		float volume = actualVolume / maxVolume;
		
		sp.play(soundId_cloud, volume, volume, 1, 0, 1f);
	}
	public void getUfoMusic(){
		AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		float actualVolume = (float) audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC);
		float maxVolume = (float) audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		float volume = actualVolume / maxVolume;
		
		sp.play(soundId_ufo, volume, volume, 1, 0, 1f);
	}

	@Override
	public void onLoadComplete(SoundPool arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}






}
