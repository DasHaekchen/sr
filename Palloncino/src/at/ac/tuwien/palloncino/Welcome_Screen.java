package at.ac.tuwien.palloncino;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.content.Intent;

/**
 * @author Maria Zisser <e1026596@student.tuwien.ac.at>, Saskia Reimerth <e0807136@student.tuwien.ac.at>
 * 
 * stellt Menue der App dar
 * von hier aus kann neues Spiel begonnen werden
 * ausserdem kann gespeicherter Highscore eingesehen 
 * und die Spielerklaerung zum Spiel geoeffnet werden
 * 
 * Quellen: Vorlesungsfolien
 * 
 */
public class Welcome_Screen extends Activity {

	private static final String TAG = Welcome_Screen.class.getSimpleName();
	private static final int HELP_DIALOG = 1;

	/**
	 * Initiates all ui elements for the welcome (main menu) screen.
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_welcome__screen);

		//initiates the start button and it's OnClickListener.
		Button start = (Button) findViewById(R.id.start_button);
		start.setOnClickListener(new OnClickListener() {

			//when pressed, the levels.class is run
			@Override
			public void onClick(View v) {
			
				Intent intent = new Intent(v.getContext(), Levels.class);
				startActivityForResult(intent, 0);

			}
		});

		//initiates the help button
		Button help_button = (Button) findViewById(R.id.help_button);
		help_button.setOnClickListener(new OnClickListener() {
			
			//when pressed, a dialog pops up
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
			
				showDialog(HELP_DIALOG);
			}
		});
		
		// initiates the highscore button
		Button highscore_button = (Button) findViewById(R.id.highscore_button);
		highscore_button.setOnClickListener(new OnClickListener() {
			
			// when clicked, the activity to show existing highscores gets started
			@Override
			public void onClick(View v) {
				Log.d(TAG, "Highscore Button clicked");
				Intent intent = new Intent(v.getContext(), ShowHighscore.class);
				startActivity(intent);
			}
		});

		
	}

	/**
	 * Creates the dialog shown when the help button gets pressed
	 * @see android.app.Activity#onCreateDialog(int)
	 * @return the dialog to be shown
	 * @deprecated the onCreateDialog is outdated
	 */
	@SuppressWarnings("deprecation")
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case HELP_DIALOG:
			Log.d(TAG, "creates help dialog");
			// build the dialog
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.helphead); // adds the title
			builder.setMessage(R.string.help_text); // adds the text
			builder.setCancelable(true);
			return builder.create();
		default:
			return super.onCreateDialog(id);
		}

	}

	/**
	 * Whenever the back button gets pressed, the app should shut down
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		finish();
	}
	
	

}
