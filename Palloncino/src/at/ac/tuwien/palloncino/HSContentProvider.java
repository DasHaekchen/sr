package at.ac.tuwien.palloncino;

import java.util.Arrays;
import java.util.HashSet;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * @author Saskia Reimerth <e0807136@student.tuwien.ac.at>
 * @see Tuwien Multimedia Reference HighscoreSQLite
 */
public class HSContentProvider extends ContentProvider {

	// OpenHandler for database
	OpenHandler dbHandler;

	private static final int HIGHSCORE = 1;
	private static final int HIGHSCORE_ID = 2;

	// authority for accessing the databse and the name of the table as base_bath 
	private static final String AUTHORITY = "at.ac.tuwien.palloncino.highscoreprovider";
	private static final String BASE_PATH = "highscore";
	
	// the resulting uri
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + BASE_PATH);

	// the uriMatcher
	private static final UriMatcher uMatch = new UriMatcher(UriMatcher.NO_MATCH);
	static {
		uMatch.addURI(AUTHORITY, BASE_PATH, HIGHSCORE);
		uMatch.addURI(AUTHORITY, BASE_PATH + "/#", HIGHSCORE_ID);
	}

	/**
	 * When the class is created, a new OpenHandler gets started
	 * @see android.content.ContentProvider#onCreate()
	 * @return true when successfull
	 */
	@Override
	public boolean onCreate() {
		dbHandler = new OpenHandler(getContext());
		return true;
	}

	/**
	 * This method finds single entries out of the table and returns them
	 * via a cursor which points to the entry
	 * @see android.content.ContentProvider#query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String)
	 * @return a Cursor which points to the entry within the table
	 */
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		// a query builder to create the needed query
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		// check if the requested columns exist
		checkColumns(projection);

		// if the columns exist, the table gets set within the query builder
		queryBuilder.setTables(HighscoreTable.DB_TABLE);

		int uriType = uMatch.match(uri);
		switch (uriType) {
		case HIGHSCORE: // do nothing => we just want to fetch all rows
			break;
		case HIGHSCORE_ID: // add row id to the query => The id is placed in the
							// uri where '#' is placed in the uri matcher
			queryBuilder.appendWhere(HighscoreTable.DB_COLUMN_ID + "="
					+ uri.getLastPathSegment());
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}

		// opens up the connection to the database
		SQLiteDatabase db = dbHandler.getWritableDatabase();

		Cursor cursor = queryBuilder.query(db, projection, selection,
				selectionArgs, null, null, sortOrder);
		// makes sure that potential listeners are getting notified
		cursor.setNotificationUri(getContext().getContentResolver(), uri);

		return cursor;
	}

	/**
	 * To insert a new entry into the table, the given values have to be transferred
	 * @see android.content.ContentProvider#insert(android.net.Uri, android.content.ContentValues)
	 * @return the uri including the id
	 */
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int uriType = uMatch.match(uri);

		// fetches the writable database from the OpenHandler dbHandler
		SQLiteDatabase db = dbHandler.getWritableDatabase();

		long id = 0;
		switch (uriType) {
		case HIGHSCORE:
			id = db.insert(HighscoreTable.DB_TABLE, null, values);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}

		// Notify all listeners about the changed entry!
		getContext().getContentResolver().notifyChange(uri, null);

		return Uri.parse(BASE_PATH + "/" + id);
	}
	
	/**
	 * Checks if the requested columns do actually exist
	 * @param projection the columns to be checked
	 */
	private void checkColumns(String[] projection) {
		String[] available = { HighscoreTable.DB_COLUMN_ID, HighscoreTable.DB_COLUMN_NAME, HighscoreTable.DB_COLUMN_PUNKTE };
		
		// if there are columns to be checked,
		if (projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
			
			// checks if all columns which are requested are available
			if (!availableColumns.containsAll(requestedColumns)) {
				throw new IllegalArgumentException("Unknown columns in projection. I know only: " + available);
			}
		}
	}
	
	/**
	 * This method is supposed to delete an entry from the database
	 * however in this project, deleting is not provided
	 * @see android.content.ContentProvider#delete(android.net.Uri, java.lang.String, java.lang.String[])
	 */
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return 0;
	}
	

	/**
	 * This method is supposed to update an entry within the table
	 * however, in this project it is not needed
	 * @see android.content.ContentProvider#update(android.net.Uri, android.content.ContentValues, java.lang.String, java.lang.String[])
	 */
	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		return 0;
	}

	/**
	 * Returns the uri type, but this method is not available in this project
	 * @see android.content.ContentProvider#getType(android.net.Uri)
	 */
	@Override
	public String getType(Uri uri) {
		return null;
	}

}
