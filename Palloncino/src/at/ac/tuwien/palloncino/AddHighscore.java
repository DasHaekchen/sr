package at.ac.tuwien.palloncino;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Saskia Reimerth <e0807136@student.tuwien.ac.at>
 * @see Tuwien Multimedia Reference HighscoreSQLite
 */
public class AddHighscore extends Activity{

	private static final String TAG = AddHighscore.class.getSimpleName();
	
	private EditText newName;
	private TextView labelScore;
	private int score;
	
	/**
	 * Creates the UI of this activity by initialising the elements in use.
	 * @param savedInstanceState the Bundle given by the caller class.
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_addhighscore);
		
		newName = (EditText)findViewById(R.id.editNewName);
		labelScore = (TextView)findViewById(R.id.labelScore);
	}
	
	/**
	 * When the "ok"-button is pressed and a name is provided,
	 * the name and score are to be saved in the database.
	 * @param v the view on which the button "ok" was pressed.
	 */
	public void onClick_OK(View v){
			
		// fetches the name from the EditText
		String name = newName.getText().toString();
		
		// if a name is entered, the values (name, score) are transmitted to the HSContentProvider
		if(!name.trim().isEmpty()){
			ContentValues values = new ContentValues();
			values.put(HighscoreTable.DB_COLUMN_NAME, name);
			values.put(HighscoreTable.DB_COLUMN_PUNKTE, score);
			
			getContentResolver().insert(HSContentProvider.CONTENT_URI, values);
			
			// afterwards the ShowHighscore activity gets started
			this.startActivity(new Intent(getApplicationContext(), ShowHighscore.class));
			this.finish();
		} else {
			// if no name is entered, a little box reminds the user to enter one
			Toast.makeText(this, R.string.msgEnterName, Toast.LENGTH_SHORT).show();
		}
	}
	
	/**
	 * When the "Highscore anzeigen"-button is pressed, the new score shall not get saved
	 * and only the existing scores are shown.
	 * @param v the view on which the button is pressed
	 */
	public void onClick_ShowHighscore(View v){
				
		// starts the showHighscoreActivity
		this.startActivity(new Intent(getApplicationContext(), ShowHighscore.class));
		this.finish();
	}
	
	/**
	 * When the activity is started by the GameOver activity
	 * it fetches the score and sets the EditBox to an empty one.
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
		
		// fetches the score which is transmitted via the intent
		score = this.getIntent().getExtras().getInt("score");
	
		// sets the TextBox to the score
		
		labelScore.setText(Integer.toString(score));
		
		// emties the EditBox (for the name)
		newName.setText("");
	}
	
	
	
	
}
