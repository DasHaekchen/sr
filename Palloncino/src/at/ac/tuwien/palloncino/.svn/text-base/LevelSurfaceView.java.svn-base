package at.ac.tuwien.palloncino;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


/**
 * @author Maria Zisser <e1026596@student.tuwien.ac.at>
 * 
 * LevelSurfaceView enthaelt Spielansicht und Spielverlauf
 * hier wird LevelThread erstellt, der fuer die Aktualisierung der Ansicht zustaendig ist
 * 
 * Quellen: Vorlesungsfolien
 */
public class LevelSurfaceView extends SurfaceView implements
SurfaceHolder.Callback {
	private static final String TAG = Welcome_Screen.class.getSimpleName();
	private static final int HELP_DIALOG = 1;

	protected float density;

	public LevelThread thread;
	protected Bitmap baloon;
	protected int xpos;
	protected int ypos;
	private Bitmap cloud, cloud2, cloud3, bird;

	protected int y;

	private long lastClick;

	private String scoreString;
	private int score = 0;

	private int lives = 3;
	protected Bitmap sonne;

	private Bitmap coin, coin2, coin3;
	protected int color;
	Paint paint = new Paint();

	protected Sprite sprite1;
	protected Sprite sprite2;
	protected LevelObject obj1;
	protected LevelObject obj2;
	private boolean intersect1 = false, intersect2 = false, intersect3 = false,
			intersect4 = false;

	protected Bitmap leftbutton;
	protected Bitmap rightbutton;
	protected Bitmap questionmark;
	private Bitmap musicon, musicoff;
	private boolean stumm;
	protected LButton left;
	protected LButton right;
	private LButton music, mute;
	private LButton help;
	private int cleft = 15, cright = 15;

	protected Levels thelevel = new Levels();
	private int levelcount;

	private long starttime = 0;
	private long timeleft, timegone;
	private long spielzeit = 30000;

	SoundPool sp;
	private Object ID_Sound1;
	
	public LevelSurfaceView(Context context) {
		super(context);
		// Log.d(TAG, "context gesetzt");
		thread = new LevelThread(null, this);
		getHolder().addCallback(this); // um Events abzupruefen
		setFocusable(true);
		density = getDensity();
		thelevel = (Levels) context;
		color = Color.rgb(158, 195, 240);
		levelcount = 1;

	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	/**
	 * Bilder fuer Level werden hier aus drawable geladen
	 * LevelObject werden fuer ersten Durchgang, 
	 * Button fuer Steuerung des Ballons
	 * Musik und Help Button erstellt sowie Thread wird gestartet
	 * */
	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		sonne = BitmapFactory.decodeResource(getResources(), R.drawable.sun);
		cloud = BitmapFactory.decodeResource(getResources(), R.drawable.cloud3);
		cloud2 = BitmapFactory
				.decodeResource(getResources(), R.drawable.cloud2);
		bird = BitmapFactory.decodeResource(getResources(), R.drawable.bird);
		coin = BitmapFactory.decodeResource(getResources(), R.drawable.coin4);
		coin2 = BitmapFactory.decodeResource(getResources(), R.drawable.coin3);
		baloon = BitmapFactory.decodeResource(getResources(),
				R.drawable.ballon_big);
		cloud3 = BitmapFactory
				.decodeResource(getResources(), R.drawable.cloud3);
		coin3 = BitmapFactory.decodeResource(getResources(), R.drawable.coin3);

		// hintergrund = new Rect(0,0,getHeight(),getWidth());
		xpos = this.getWidth() / 2;
		ypos = this.getHeight() - baloon.getHeight() - 60;

		// hintergrund = new Rect(0,0, getDisplay().getHeight(),
		// getDisplay().getWidth());
		sprite1 = new Sprite(this, coin);
		sprite2 = new Sprite(this, coin3);
		obj1 = new LevelObject(this, cloud3);
		obj2 = new LevelObject(this, cloud2);

		leftbutton = BitmapFactory.decodeResource(getResources(),
				R.drawable.left);
		rightbutton = BitmapFactory.decodeResource(getResources(),
				R.drawable.right);
		left = new LButton(leftbutton.getWidth(), leftbutton.getHeight(),
				leftbutton);
		left.setPosition(getWidth() / 5 * 2 - leftbutton.getWidth(),
				getHeight() - leftbutton.getHeight() - 10);
		right = new LButton(rightbutton.getWidth(), rightbutton.getHeight(),
				rightbutton);
		right.setPosition(getWidth() / 5 * 4 - leftbutton.getWidth(),
				getHeight() - leftbutton.getHeight() - 10);

		musicon = BitmapFactory.decodeResource(getResources(),
				R.drawable.musicon);
		musicoff = BitmapFactory.decodeResource(getResources(),
				R.drawable.musicoff);
		music = new LButton(musicon.getWidth(), musicon.getHeight(), musicon);
		music.setPosition(getWidth() / 2 + 20 * density, 12 * density);
		mute = new LButton(musicon.getWidth(), musicon.getHeight(), musicoff);
		mute.setPosition(getWidth() / 2 + 20 * density, 12 * density);

		questionmark = BitmapFactory.decodeResource(getResources(),
				R.drawable.questionmark);
		help = new LButton(questionmark.getWidth(), questionmark.getHeight(),
				questionmark);
		help.setPosition(getWidth() / 2 + musicon.getWidth() + 30 * density,
				12 * density);

		starttime = System.currentTimeMillis();

		
		
		y = 0;
		thread = new LevelThread(getHolder(), this); // gameloop erstellen
		thread.setRunning(true);
		if (thread.getState() == thread.getState().NEW) {
			thread.start();
		}

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		thread.setRunning(false);
		boolean retry = true;
		while (retry) {
			try {
				
				thread.interrupt();
				thread.join();
				retry = false;
			} catch (Exception e) {
				e.getMessage();
				e.getStackTrace();
			}
		}
	}

	/**
	 * wird auf Display getippt, werden x und y Koordinaten ermittelt
	 * je nachdem wo getippt wurde, wird entprechende Aktion ausgefuehrt:
	 * 
	 * Musik an/aus : auf ImageButton mit Musikzeichen getippt
	 * Hilfetext anzeigen: auf HelpButton getippt
	 * Spiel pausieren: im Displaybereich zwischen Buttons und Punkteanzeige getippt
	 * Steuerung des Ballons links/rechts: auf entsprechenden Pfeilbutton getippt
	 * 
	 * */
	public boolean onTouchEvent(MotionEvent event) {

		synchronized (getHolder()) {
			float x = event.getX();
			float y = event.getY();
			if (System.currentTimeMillis() - lastClick > 300) {
				lastClick = System.currentTimeMillis();
				if (music.rect.contains(x, y)) {
					thelevel.musicOnOff();
					return true;
				}
			}
			if (help.rect.contains(x, y)) {
				thelevel.showHelp();
				if (thelevel.getPause()){
					return true;
				}
			}

			if (right.rect.contains(x, y) && !thelevel.getPause()) {

				if (xpos + cright < getWidth() - baloon.getWidth()) {
					xpos += cright;
					return true;
				}
			}

			if (left.rect.contains(x, y) && !thelevel.getPause()) {

				if (xpos - cleft > baloon.getWidth()) {
					xpos -= cleft;
					return true;
				}

			}
			if (y < getHeight() - leftbutton.getHeight() - 100
					&& y > musicoff.getHeight() + 10 || help.rect.contains(x, y)) {
				thelevel.pauseLevel();
			}

			return super.onTouchEvent(event);
		}
	}

	/**
	 * zeichnet Ballon, LevelObjekte, Punkteanzeige, Lebenanzeige, Buttons auf Canvas
	 * ueberprueft ob LevelObjekte schon am Ende des Displays sind,
	 * wenn ja, werden neue LevelObjekte aufgerufen.
	 * ueberprueft, ob Objekte und Ballon sich ueberschneiden. wenn ja werden entsprechende
	 * Aktionen ausgefuehrt
	 * 
	 * Ballon schneidet Wolke, Vogel: ein Leben wird abegezogen
	 * Ballon schneidet Muenze: Punkte werden erhoeht
	 * 
	 * nach bestimmter Zeit, die abgelaufen ist, wird naechste Levelansicht gestartet
	 * */
	@Override
	public void onDraw(Canvas canvas) {// zeichnen am Canvas
		super.onDraw(canvas);

		canvas.drawColor(color); // zeichnet Hintergrund
		left.draw(canvas);
		right.draw(canvas);
		if (stumm) {
			mute.draw(canvas);
		} else {
			music.draw(canvas);
		}

		help.draw(canvas);

		final int fontSize = (int) (20 * density); // zeichnet Punkte
		int yTextPos = (int) (35 * density) + sonne.getHeight();
		Typeface font = Typeface.create("Arial", Typeface.NORMAL);

		if (levelcount == 1) {
			paint.setColor(Color.BLACK);
		}
		paint.setTypeface(font);
		paint.setTextSize(fontSize);
		paint.setAntiAlias(true);
		scoreString = String.valueOf(score);
		int xtext = (int) (20 * density);
		final String text = scoreString;
		canvas.drawText(text, xtext, yTextPos, paint);

		canvas.drawBitmap(baloon, xpos, ypos, null);

		drawSun(canvas, paint);

		if (obj1.getUnten() == false) {
			obj1.onDraw(canvas);
		} else {
			obj1 = null;
			int r = (int) (Math.random() * 3 + 1);

			switch (r) {
			case 1:
				obj1 = getNewObject(cloud2);
				break;
			case 2:
				obj1 = getNewObject(cloud3);
				break;
			case 3:
				obj1 = getNewObject(bird);
				break;
			default:
				obj1 = getNewObject(cloud);
			}
		}

		if (obj2.getUnten() == false) {
			obj2.onDraw(canvas);
		} else {
			obj2 = null;
			int r = (int) (Math.random() * 3 + 1);

			switch (r) {
			case 1:
				obj2 = getNewObject(cloud2);
				break;
			case 2:
				obj2 = getNewObject(cloud3);
				break;
			case 3:
				obj2 = getNewObject(bird);
				break;
			default:
				obj2 = getNewObject(bird);
			}

		}

		if (sprite1.getUnten() == false) {
			sprite1.onDraw(canvas);
		} else {
			sprite1 = null;
			int r = (int) (Math.random()*2 + 1);
			if (r == 1) {
				sprite1 = getNewSprite(coin);
			}
			if (r == 2) {
				sprite1 = getNewSprite(coin3);
			}
			if (r == 3){
				sprite1 = getNewSprite(coin2);
			}
		}
		if (sprite2.getUnten() == false) {
			sprite2.onDraw(canvas);
		} else {
			sprite2 = null;
			int r = (int) (Math.random()*2 + 1);
			if (r == 1) {
				sprite2 = getNewSprite(coin3);
			}
			if (r == 2) {
				sprite2 = getNewSprite(coin);
			}
			if (r == 3){
				sprite2 = getNewSprite(coin3);
			}
		}

		intersect1 = intersect(obj1.getX(), obj1.getY(), obj1.getXunten(),
				obj1.getYunten(), xpos, ypos, (xpos + baloon.getWidth()),
				(ypos + baloon.getHeight()));
		intersect2 = intersect(obj2.getX(), obj2.getY(), obj2.getXunten(),
				obj2.getYunten(), xpos, ypos, (xpos + baloon.getWidth()),
				(ypos + baloon.getHeight()));
		intersect3 = intersect(sprite1.getX(), sprite1.getY(),
				sprite1.getXunten(), sprite1.getYunten(), xpos, ypos,
				(xpos + baloon.getWidth()), (ypos + baloon.getHeight()));
		intersect4 = intersect(sprite2.getX(), sprite2.getY(),
				sprite2.getXunten(), sprite2.getYunten(), xpos, ypos,
				(xpos + baloon.getWidth()), (ypos + baloon.getHeight()));

		if (intersect1) {
			if(obj1.getBild()== bird){
				thelevel.getCoinMusik();
			}
			else{thelevel.getCloudMusic();}
			lives--;
			obj1.setUnten(true);
			if (isGameOver()) {
				thread.setRunning(false);
				thelevel.onGameOver();
			}
		}
		if (intersect2) {
			if(obj2.getBild() == bird){
				thelevel.getCoinMusik();
			}
			else{thelevel.getCloudMusic();}
			
			lives--;
			obj2.setUnten(true);
			if (isGameOver()) {
				thread.setRunning(false);
				thelevel.onGameOver();
			}
		}
		if (intersect3) {
			score += 10;
			thelevel.getCoinMusik();
			sprite1.setUnten(true);
		}

		if (intersect4) {
			score += 10;

			thelevel.getCoinMusik();
			sprite2.setUnten(true);
		}
		// Log.v(TAG, "zeit"+timeleft/1000);
		timegone = System.currentTimeMillis() - starttime;
		timeleft = spielzeit - timegone + thelevel.getPauseTime();
		if (timeleft <= 0 && levelcount == 1) {
			levelcount++;

			secondLevel();

		}
		else if (timeleft <= 0 && levelcount ==2) {
			thread.setRunning(false);
			levelcount++;
			thelevel.win();
		}
	}

	/**
	 * @param AMin, AMax minimale und maximale Punkte(Ma�e) des ersten Bildes
	 * 		BMin, BMax minimale und maximale Punkte(Ma�e) des ersten Bildes
	 * 
	 * @see http://stackoverflow.com/questions/17392379/how-to-make-a-simple-collision-detection-of-bitmaps-in-android
	 * */

	boolean intersect(int AMinX, int AMinY, int AMaxX, int AMaxY,
			int BMinX, int BMinY, int BMaxX, int BMaxY) {
		assert (AMinX < AMaxX);
		assert (AMinY < AMaxY);
		assert (BMinX < BMaxX);
		assert (BMinY < BMaxY);

		if ((AMaxX < BMinX) || 
				(BMaxX < AMinX) || 
				(AMaxY < BMinY) ||
				(BMaxY < AMinY)) 
		{
			return false;
		}

		return true;
	}

	/**
	 * abfragen, um Display unabhaengige Darstellung zu erreichen
	 * teilweise in App eingesetzt
	 * */
	public float getDensity() {
		density = getResources().getDisplayMetrics().density;
		return density;
	}

	/**
	 * zeichnet je nach Anzahl der Leben am oberen Rand des Displays entsprechend Sonnen
	 * */
	private void drawSun(Canvas c, Paint p) { // zeichnet vorhandene "Leben"
		int x = (int) (15 * density);
		int y = (int) (12 * density);

		if (lives == 3) {
			c.drawBitmap(sonne, x, y, p);
			c.drawBitmap(sonne, x + sonne.getWidth() + 3 * density, y, p);
			c.drawBitmap(sonne, x + 2 * sonne.getWidth() + 6 * density, y, p);
		}

		if (lives == 2) {
			c.drawBitmap(sonne, x, y, p);
			c.drawBitmap(sonne, x + sonne.getWidth() + 3 * density, y, p);
		}

		if (lives == 1) {
			c.drawBitmap(sonne, x, y, p);
		}
	}

	/**
	 * wei�t definierten Bildern neue Bilder zu, um Ansicht zu aendern
	 * setzt Spielzeit neu
	 * */
	private void secondLevel() {
		obj1.setUnten(true);
		obj2.setUnten(true);
		cloud = BitmapFactory.decodeResource(getResources(), R.drawable.planet);
		cloud2 = BitmapFactory.decodeResource(getResources(), R.drawable.ufo);
		bird = BitmapFactory.decodeResource(getResources(), R.drawable.planet1);
		paint.setColor(Color.WHITE);
		cloud3 = BitmapFactory.decodeResource(getResources(),
				R.drawable.planet3);
		color = Color.rgb(0, 0, 85);
		spielzeit = 30000;
		starttime = System.currentTimeMillis();
	}

	/**
	 * pausiert LevelThread
	 * */
	public void pauseThread() {
		thread.setRunning(false);
	}

	/**
	 * startet neuen Thread
	 * */
	public void resumeThread() {
		thread = new LevelThread(getHolder(), this);
		thread.setRunning(true);
		thread.start();
	}

	public boolean runningThread() {
		return thread.isRunning();
	}

	public boolean isGameOver() {
		return lives <= 0;
	}

	/**
	 * @param b...Bild das neuer Sprite enthalten soll
	 *@return new Sprite, neuer Sprite mit uebergebenen Bild
	 * (eigene Methode, um nicht in onDraw() new Funktion aufrufen zu m�ssen,
	 * gab Probleme beim Testen)
	 * */
	private Sprite getNewSprite(Bitmap b) {
		return new Sprite(this, b);
	}

	/**
	 * @param b...Bild das neuer Sprite enthalten soll
	 *@return new Sprite, neuer Sprite mit uebergebenen Bild
	 *(eigene Methode, um nicht in onDraw() new Funktion aufrufen zu m�ssen,
	 * gab Probleme beim Testen)
	 * */
	private LevelObject getNewObject(Bitmap b) {
		return new LevelObject(this, b);
	}

	/**Getter und Setter fuer Variablen*/
	public void setLeft() {
		cleft++;
	}

	public void setRight() {
		cright++;
	}

	public void setStumm(boolean b) {
		stumm = b;
	}

	public boolean getStumm() {
		return stumm;
	}

	public int getScore() {
		return this.score;
	}

}
