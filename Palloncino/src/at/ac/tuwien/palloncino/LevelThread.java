package at.ac.tuwien.palloncino;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * @author Maria Zisser <e1026596@student.tuwien.ac.at>
 * 
 * zeichnet LevelSurfaceView nach Abhaengigkeit der Frames
 */
public class LevelThread extends Thread {
	static final long FPS = 30;
	private SurfaceHolder surfaceHolder;
	private LevelSurfaceView myView;
	private boolean running;
	private Sprite spriteAnimationPanel;
	
	/*
	 * 
	 */
	public void setRunning(boolean running){
		this.running = running;
	}
	public boolean isRunning(){
		return running;
	}
	public LevelThread(SurfaceHolder surfaceHolder, LevelSurfaceView v) {
		super();
		myView = v;
		this.surfaceHolder = surfaceHolder;
		
	
	}


	/**
	 * starts the thread
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		//frameabhaengigkeit hinzufuegen
		long TPS = 1000/FPS;
		long startTime, sleepTime;
		
		while (running) {
			Canvas c=null; //neuzeichnen
			startTime = System.currentTimeMillis();
			try{
				c = this.surfaceHolder.lockCanvas(null);
				synchronized (this.surfaceHolder){
					if(c!=null){	
						myView.onDraw(c);
					}
				}
			}
			finally{
				if (c != null){
					this.surfaceHolder.unlockCanvasAndPost(c);
				}	
			}
			sleepTime = TPS - (System.currentTimeMillis()-startTime);
			try{
				if(sleepTime>0){
					sleep(sleepTime);
				}
				else{
					sleep(25);
				}
			}catch(Exception e){}
		}
	}
	


}